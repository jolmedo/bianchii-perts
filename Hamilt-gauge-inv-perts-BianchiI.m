(* ::Package:: *)

(* ::Title:: *)
(*Code to calculate second order Hamiltonian for Bianchi I spacetime*)


(* ::Text:: *)
(*This code uses the Hamiltonian formulation of general relativity to derive the second - order Hamiltonian governing perturbations in a system involving a minimally coupled single canonical scalar field and gravity. This code is largely based on the work by D.Langlois, Class.Quantum Grav.11, 389 (1994) with the difference that instead of ignoring the gauge dependent part of perturbations (two scalars and two vector perturbations), we show that they decouple from the gauge independent part of the perturbations. This is important in the case of complex systems, such as in Bianchi I where in the perturbations are coupled to each other. In this particular example, we will focus on case of Bianchi I spacetime.*)


(* ::Subtitle:: *)
(*General set up*)


(* ::Text:: *)
(*This part is common for any space - times.Here, we first load the xPert package and then define the manifold, metric and various functions defined in that manifold.We finally derive the Hamiltonian and diffeomorphism constraint for any order in perturbation.*)


(* ::Input:: *)
(*<<xAct`xPert`;*)


(* ::Section:: *)
(*Define manifold and perturbations*)


(* ::Input:: *)
(*DefManifold[M3,3,{a,b,c,d,e,f,i,j,l,m,n,o,p,q,r,u,v,w}]*)
(*(*Spatial maniforld M3 with dimension 3 and index labels*)*)


(* ::Input:: *)
(*DefParameter[t,PrintAs->"t"]*)
(*(*time parameter*)*)


(* ::Input:: *)
(*DefConstantSymbol[\[Kappa]]*)
(*(*\[Kappa]=8\[Pi]G*)*)


(* ::Input:: *)
(*DefMetric[1,h[-i,-j],CD,{";","D"},OtherDependencies->{t},WeightedWithBasis->AIndex];*)
(*(*Metric Subscript[h,ij] with covariant derivative CD denoted by a ";" or a "D". Metric also depends on a parameter t. Note that this also defines various quantities associated with the metric,such as Christoffel symbol,Reimann and Ricci tensor,Ricci scalar etc..*)*)
(**)


(* ::Text:: *)
(*We define a function to collect and canonicalize equal-order terms:*)


(* ::Input:: *)
(*org[expr_]:=Collect[ContractMetric[expr],$PerturbationParameter,ToCanonical]*)
(*(*This function will help in simplifying the expressions.*)*)


(* ::Input:: *)
(*DefMetricPerturbation[h,\[Delta]h,\[Epsilon]]*)
(*(*Define perturbation \[Delta]h to the h metric.*)*)


(* ::Input:: *)
(*DefTensor[\[Phi][],{M3,t}]*)
(*(*Scalar field,\[Phi](x,t)*)*)


(* ::Input:: *)
(*DefScalarFunction[V]*)
(*ConformalWeight[V']=0;*)
(*(*Define V(\[Phi])*)*)


(* ::Input:: *)
(*(*time derivatives can be taken as a ParamD using parameter t defined below*)*)


(* ::Input:: *)
(*DefTensorPerturbation[\[Delta]\[Phi][LI[1]],\[Phi][],{M3,t}]*)
(*(*Define first-order perturbation \[Delta]\[Phi](t,x)*)*)


(* ::Input:: *)
(*(*UndefTensor[ExK]*)*)


(* ::Input:: *)
(*DefTensor[P[i,j],{M3,t}(*,Symmetric[{-a,-b}]*)]*)
(*(*momentum conjugate to the metric*)*)


(* ::Input:: *)
(*DefTensorPerturbation[\[Delta]P[LI[1],a,b],P[a,b],{M3,t}]*)
(*(*Define first-order momentum perturbation*)*)


(* ::Input:: *)
(*DefTensor[P\[Phi][],{M3,t}(*,Symmetric[{-a,-b}]*)]*)
(*(*Momentum conjugate to \[Phi]*)*)


(* ::Input:: *)
(*DefTensorPerturbation[\[Delta]P\[Phi][LI[1]],P\[Phi][],{M3,t}]*)
(*(*first-order perturbation of momentum conjugate to \[Phi]*)*)


(* ::Subsubsection:: *)
(*Poisson bracket*)


(* ::Input:: *)
(*PoissonBracket[f_,g_,q_List,p_List]/;Length[q]==Length[p]:=D[f,{q}].D[g,{p}]-D[f,{p}].D[g,{q}]*)
(*PoissonBracket[most__,q:Except[_List],p_]:=PoissonBracket[most,{q},p]*)
(*PoissonBracket[most__,p:Except[_List]]:=PoissonBracket[most,{p}]*)


(* ::Text:: *)
(* *)


(* ::Section:: *)
(*Hamiltonian up to second order in an arbitrary space - time*)


(* ::Text:: *)
(*Now we will use the quantities defined above to define a Hamiltonian in the manifold. We do so by defining different terms in the Hamiltonian and then putting them together.*)


(* ::Subsection:: *)
(*Diffeomorphism constraint*)


(* ::Input:: *)
(*diffeo=-2PD[-a]@(h[-i,-j]P[j,a])+P[l,m]PD[-i]@h[-l,-m]+P\[Phi][]PD[-i]@\[Phi][]*)


(* ::Input:: *)
(*diffeo1=(Perturbed[diffeo,1]/\[Epsilon])/.MakeRule[{PD[-a]@\[Phi][],0}]/.MakeRule[{PD[-a]@h[-i,-j],0}]/.MakeRule[{PD[-a]@P[i,j],0}]*)
(*(*First order diffeomorphism constraint*)*)


(* ::Subsection:: *)
(*Different terms in the scalar constraint*)


(* ::Input:: *)
(*ricciscalar=(*ChangeCovD[*)h[a,b]RiemannCD[-a,-c,-b,c]//RiemannToChristoffel//ChristoffelToMetric(*,PD,CD]*)//Simplification//NoScalar*)
(*(*ricci scalar*)*)


(* ::Input:: *)
(*Perturbed[(Deth[])^(1/2) ricciscalar,2]/.MakeRule[{PD[-a]@h[-b,-c],0}]//ExpandPerturbation;*)
(*r2=%/.MakeRule[{h[LI[2],-a,-b],0}]*)
(*(*ricci scalar up to second-order in perturbations.*)*)


(* ::Input:: *)
(*pipi=ExpandPerturbation[Perturbed[(Deth[])^(-1/2) (P[a,b]P[c,d]h[-a,-c]h[-b,-d]-P[a,b]h[-a,-b]P[c,d]h[-c,-d]/2),2]]/.MakeRule[{\[Delta]P[LI[2],-a,-b],0}]/.MakeRule[{\[Delta]h[LI[2],-a,-b],0}]*)
(*(*P^abSubscript[P, ab]-P^2/2*)*)


(* ::Input:: *)
(*matterterms=ExpandPerturbation[Perturbed[1/2*Deth[]^(-1/2)*P\[Phi][]^2+Sqrt[Deth[]]*V[\[Phi][]]+Deth[]^(1/2)/2*PD[-a]@\[Phi][]PD[-b]@\[Phi][]h[a,b],2]]/.MakeRule[{\[Delta]h[LI[2],-a,-b],0}]/.MakeRule[{\[Delta]\[Phi][LI[2]],0}]/.MakeRule[{\[Delta]P\[Phi][LI[2]],0}]/.MakeRule[{PD[-a]@\[Phi][],0}]*)


(* ::Subsubsection:: *)
(*Full Hamiltonian up to second order*)


(* ::Input:: *)
(*S=Series[2*\[Kappa]*pipi-1/(2*\[Kappa])*r2+matterterms,{\[Epsilon],0,2}]*)


(* ::Subtitle:: *)
(*Bianchi - I spacetime*)


(* ::Section:: *)
(*Hamiltonian at zeroth-order*)


(* ::Input:: *)
(*DefTensor[\[Sigma][a,b],{M3,t},Symmetric[{a,b}]]*)
(*(*Define shear tensor. The shear that will appear in this notebook will be suitably scaled by scalefactor, so that it transforms in a covariant manner.*)*)


(* ::Input:: *)
(*DefTensor[\[Sigma]b[],{M3,t}]*)
(*(*\[Sigma]b^2 = \[Sigma]^abSubscript[\[Sigma], ab].*)*)


(* ::Input:: *)
(*AutomaticRules[\[Sigma],MakeRule[{\[Sigma][a,b]h[-a,-b],0}]]*)
(*AutomaticRules[\[Sigma],MakeRule[{\[Sigma][a,-a],0}]]*)
(*AutomaticRules[\[Sigma],MakeRule[{\[Sigma][a,b]\[Sigma][c,d]h[-a,-c]h[-b,-d],\[Sigma]b[]^2}]]*)
(*(*Shear tensor is traceless*)*)


(* ::Input:: *)
(*(*AutomaticRules[\[Sigma],MakeRule[{\[Sigma][a,b]\[Sigma][-a,-b],\[Sigma]b[]^2}]]*)*)


(* ::Input:: *)
(*DefTensor[\[Pi]a[],{M3,t}]*)
(*(*Define \[Pi]a. Note that in Bianchi I spacetime, a and \[Pi]a are not the phase space variables, they are instead the three scalefactors and their conjugate momenta.*)*)


(* ::Input:: *)
(*S0=(*Detg[]^(1/6)*)SeriesCoefficient[S,0]*)
(*(*Zeroth-order Hamiltonian for any spacetime. Now, we need to substitute the expression for momentum conjugate to the metric.*)*)


(* ::Input:: *)
(*(*DefTensor[Hp[],{M3,t}]*)*)


(* ::Input:: *)
(*bgmomrule=MakeRule[{P[a,b],\[Pi]a[]/6  Deth[]^(1/6) h[a,b]+Deth[]^(1/2)/(2\[Kappa]) \[Sigma][a,b]/(*Deth[]^(1/6)*)1}]*)
(*(*the momentum conjugate to the metric in Bianchi I spacetime.*)*)


(* ::Input:: *)
(*S0a=S0/.bgmomrule//ToCanonical*)
(*(*Zeroth-order Hamiltonian in Bianchi-I spacetime.*)*)


(* ::Input:: *)
(*DefTensor[sf[],{M3,t}]*)
(*(*sf : scale factor*)*)


(* ::Input:: *)
(*sfrule=MakeRule[{Deth[],sf[]^6}]*)


(* ::Input:: *)
(*S0aintermsofsf=S0a/.sfrule//org*)


(* ::Input:: *)
(*PoissonBracket[sf[],S0aintermsofsf,{sf[],\[Phi][]},{\[Pi]a[],P\[Phi][]}]*)


(* ::Section:: *)
(*First order constraints*)


(* ::Subsection:: *)
(*Diffeomorphism constraint at first-order*)


(* ::Input:: *)
(*diffeo1*)
(*(*Eq. A2*)*)


(* ::Input:: *)
(*diffeoa=diffeo1/.bgmomrule//ToCanonical*)


(* ::Subsection:: *)
(*Hamiltonian constraint at first-order*)


(* ::Input:: *)
(*S1a=(*Deth[]^(1/6)*)SeriesCoefficient[S,1]*)
(*(*First-order Hamiltonian constraint for arbitrary spacetime.*)*)


(* ::Input:: *)
(*S1b=S1a/.bgmomrule//ToCanonical*)
(*(*Substitute background momenta *)*)


(* ::Input:: *)
(*S1a//org*)
(*(*Eq. A1*)*)


(* ::Section:: *)
(**)
(*Scalar-Vector-Tensor decomposition*)


(* ::Subsection:: *)
(*Basis vectors in fourier space*)


(* ::Input:: *)
(*(*Let us define three-orthogonal vectors kv,e1 and e2 in fourier space.These vectors can be used to construct six orthogonal basis matrices,using which we can decompose the metric and its momentum perturbations. The resulting components,depending on how they transform when rotated about kv,are known as scalar, vector and tensor perturbations.*)*)


(* ::Input:: *)
(*DefTensor[kv[-a],{M3,t}]*)
(*DefTensor[e1[-a],{M3,t},OrthogonalTo->kv[a]]*)
(*DefTensor[e2[-a],{M3,t},OrthogonalTo->{kv[a],e1[a]}]*)
(*(*Define,kv,e1 and e2 orthogonal to each other.*)*)


(* ::Input:: *)
(*DefTensor[k[],{M3,t}]*)
(*(*Define k,intended to be the norm of vector kv*)*)


(* ::Input:: *)
(*AutomaticRules[kv,MakeRule[{kv[-a]kv[-b]h[a,b],k[]^2}]]*)
(*AutomaticRules[e1,MakeRule[{e1[-a]e1[-b]h[a,b],1}]]*)
(*AutomaticRules[e2,MakeRule[{e2[-a]e2[-b]h[a,b],1}]]*)
(*(*Normalisation conditions for kv,e1 and e2.*)*)


(* ::Input:: *)
(*AutomaticRules[e1,MakeRule[{e1[-a]e1[a],1}]]*)
(*AutomaticRules[e2,MakeRule[{e2[-a]e2[a],1}]]*)
(*AutomaticRules[kv,MakeRule[{kv[-a]kv[a],k[]^2}]]*)
(*(*Normalisation conditions for kv,e1 and e2.*)*)


(* ::Input:: *)
(*DefTensor[A1[-a,-b],{M3,t},Symmetric[{-a,-b}]]*)
(*DefTensor[A2[-a,-b],{M3,t},Symmetric[{-a,-b}]]*)
(*DefTensor[A3[-a,-b],{M3,t},Symmetric[{-a,-b}]]*)
(*DefTensor[A4[-a,-b],{M3,t},Symmetric[{-a,-b}]]*)
(*DefTensor[A5[-a,-b],{M3,t},Symmetric[{-a,-b}]]*)
(*DefTensor[A6[-a,-b],{M3,t},Symmetric[{-a,-b}]]*)
(*(*Six orthogonal matrices,the first one being the metric itself.*)*)


(* ::Input:: *)
(*DefTensor[\[Gamma]1[],{M3,t}]*)
(*DefTensor[\[Gamma]2[],{M3,t}]*)
(*DefTensor[\[Gamma]3[],{M3,t}]*)
(*DefTensor[\[Gamma]4[],{M3,t}]*)
(*DefTensor[\[Gamma]5[],{M3,t}]*)
(*DefTensor[\[Gamma]6[],{M3,t}]*)
(*DefTensor[\[Pi]1[],{M3,t}]*)
(*DefTensor[\[Pi]2[],{M3,t}]*)
(*DefTensor[\[Pi]3[],{M3,t}]*)
(*DefTensor[\[Pi]4[],{M3,t}]*)
(*DefTensor[\[Pi]5[],{M3,t}]*)
(*DefTensor[\[Pi]6[],{M3,t}]*)
(*(*Components of Subscript[\[Delta]h,ij] and \[Delta]\[Pi]^ij along the basis matrices.*)*)


(* ::Input:: *)
(*AutomaticRules[A2,MakeRule[{A2[a,-a],0}]]*)
(*AutomaticRules[A3,MakeRule[{A3[a,-a],0}]]*)
(*AutomaticRules[A4,MakeRule[{A4[a,-a],0}]]*)
(*AutomaticRules[A5,MakeRule[{A5[a,-a],0}]]*)
(*AutomaticRules[A6,MakeRule[{A6[a,-a],0}]]*)
(*AutomaticRules[A1,MakeRule[{A1[a,b]A2[-a,-b],0}]]*)
(*AutomaticRules[A1,MakeRule[{A1[a,b]A3[-a,-b],0}]]*)
(*AutomaticRules[A1,MakeRule[{A1[a,b]A4[-a,-b],0}]]*)
(*AutomaticRules[A1,MakeRule[{A1[a,b]A5[-a,-b],0}]]*)
(*AutomaticRules[A1,MakeRule[{A1[a,b]A6[-a,-b],0}]]*)
(*AutomaticRules[A2,MakeRule[{A2[a,b]A3[-a,-b],0}]]*)
(*AutomaticRules[A2,MakeRule[{A2[a,b]A4[-a,-b],0}]]*)
(*AutomaticRules[A2,MakeRule[{A2[a,b]A5[-a,-b],0}]]*)
(*AutomaticRules[A2,MakeRule[{A2[a,b]A6[-a,-b],0}]]*)
(*AutomaticRules[A3,MakeRule[{A3[a,b]A4[-a,-b],0}]]*)
(*AutomaticRules[A3,MakeRule[{A3[a,b]A5[-a,-b],0}]]*)
(*AutomaticRules[A3,MakeRule[{A3[a,b]A6[-a,-b],0}]]*)
(*AutomaticRules[A4,MakeRule[{A4[a,b]A5[-a,-b],0}]]*)
(*AutomaticRules[A4,MakeRule[{A4[a,b]A6[-a,-b],0}]]*)
(*AutomaticRules[A5,MakeRule[{A5[a,b]A6[-a,-b],0}]]*)
(*AutomaticRules[A2,MakeRule[{A2[a,b]A2[-a,-b],1 (*2/3*)}]]*)
(*AutomaticRules[A3,MakeRule[{A3[a,b]A3[-a,-b],1}]]*)
(*AutomaticRules[A4,MakeRule[{A4[a,b]A4[-a,-b],1}]]*)
(*AutomaticRules[A5,MakeRule[{A5[a,b]A5[-a,-b],1}]]*)
(*AutomaticRules[A6,MakeRule[{A6[a,b]A6[-a,-b],1}]]*)
(*(*Orthogonality and normalization of basis matrices.*)*)


(* ::Input:: *)
(*AutomaticRules[A3,MakeRule[{A3[a,b]kv[-a]kv[-b],0}]]*)
(*AutomaticRules[A4,MakeRule[{A4[a,b]kv[-a]kv[-b],0}]]*)
(*AutomaticRules[A5,MakeRule[{A5[a,b]kv[-a],0}]]*)
(*AutomaticRules[A6,MakeRule[{A6[a,b]kv[-a],0}]]*)
(*(*Impose transverse nature of some of the basis matrices.*)*)


(* ::Input:: *)
(*A1rule=MakeRule[{A1[-a,-b],h[-a,-b]/Sqrt[3]}]*)


(* ::Input:: *)
(*A2rule=MakeRule[{A2[-a,-b],Sqrt[3/2](kv[-a]kv[-b]/k[]^2-h[-a,-b]/3)}]*)
(*(*Explicit definition of A2.Since,A1 and A2 do not transform under rotations about the kv,the correponding components are known as scalars.Hence,\[Gamma]1,\[Gamma]2,\[Pi]1,\[Pi]2 are known as scalar modes.*)*)


(* ::Input:: *)
(*A3rule=MakeRule[{A3[-a,-b],(kv[-a]/k[]e1[-b]+kv[-b]/k[]e1[-a])/Sqrt[2]}]*)
(*(*Explicit definition of A3*)*)


(* ::Input:: *)
(*A4rule=MakeRule[{A4[-a,-b],(kv[-a]/k[]e2[-b]+kv[-b]/k[]e2[-a])/Sqrt[2]}]*)
(*(*Explicit definition of A4*)*)


(* ::Input:: *)
(*A5rule=MakeRule[{A5[-a,-b],(e1[-a]e1[-b]-e2[-a]e2[-b])/Sqrt[2]}]*)
(*(*Explicit definition of A5*)*)


(* ::Input:: *)
(*A6rule=MakeRule[{A6[-a,-b],(e1[-a]e2[-b]+e1[-b]e2[-a])/Sqrt[2]}]*)
(*(*Explicit definition of A6*)*)


(* ::Input:: *)
(*(*Define 'components' of shear along the basis matrices.*)*)
(*(*DefTensor[\[Sigma]p[],{M3,t}]*)
(*DefTensor[\[Sigma]Tp[],{M3,t}]*)
(*DefTensor[\[Sigma]Tc[],{M3,t}]*)
(*DefTensor[\[Sigma]V1[],{M3,t}]*)
(*DefTensor[\[Sigma]V2[],{M3,t}]*)*)


(* ::Input:: *)
(*DefTensor[\[Gamma]0[],{M3,t}]*)
(*DefTensor[\[Pi]0[],{M3,t}]*)


(* ::Input:: *)
(*\[Gamma]0rule=MakeRule[{\[Delta]\[Phi][LI[1]],\[Gamma]0[]/Sqrt[4\[Kappa]]}]*)


(* ::Input:: *)
(*\[Pi]0rule=MakeRule[{\[Delta]P\[Phi][LI[1]],\[Pi]0[]*Sqrt[4\[Kappa]]}]*)


(* ::Input:: *)
(*moderule1=MakeRule[{\[Delta]h[LI[1],-a,-b],\[Gamma]1[]A1[-a,-b]+\[Gamma]2[]A2[-a,-b]+\[Gamma]3[]A3[-a,-b]+\[Gamma]4[]A4[-a,-b]+\[Gamma]5[]A5[-a,-b]+\[Gamma]6[]A6[-a,-b]}]*)
(*moderule2=MakeRule[{\[Delta]P[LI[1],a,b],\[Pi]1[]A1[a,b]+(*3/2*)\[Pi]2[]A2[a,b]+\[Pi]3[]A3[a,b]+\[Pi]4[]A4[a,b]+\[Pi]5[]A5[a,b]+\[Pi]6[]A6[a,b]}]*)
(*(*Mode decomposition of the metric perturbation and its conjugate momentum.*)*)


(* ::Input:: *)
(*DefTensor[\[Sigma]2[],{M3,t}]*)
(*DefTensor[\[Sigma]3[],{M3,t}]*)
(*DefTensor[\[Sigma]4[],{M3,t}]*)
(*DefTensor[\[Sigma]5[],{M3,t}]*)
(*DefTensor[\[Sigma]6[],{M3,t}]*)


(* ::Input:: *)
(*sheardecomposition=MakeRule[{\[Sigma][-a,-b],((*3/2*)\[Sigma]2[]A2[-a,-b]+(*Sqrt[2]*)\[Sigma]3[]A3[-a,-b]+(*Sqrt[2]*)\[Sigma]4[]A4[-a,-b]+\[Sigma]5[]A5[-a,-b]+\[Sigma]6[]A6[-a,-b])}]*)
(*(*Shear is "decomposed" in to components.*)*)


(* ::Input:: *)
(*sigmasq=\[Sigma][-i,-j]\[Sigma][i,j]/.sheardecomposition//org*)
(*(*\[Sigma]^2 in terms of components.*)*)


(* ::Input:: *)
(*\[Sigma]brule=MakeRule[{\[Sigma]b[],sigmasq^(1/2)}]*)
(*(*\[Sigma]^2 in terms of components.*)*)


(* ::Input:: *)
(*\[Sigma][-i,-j]A4[i,j]/.sheardecomposition//org*)


(* ::Input:: *)
(*bgconstraintrule=MakeRule[{V[\[Phi][]],-1/Deth[]^(1/2) (P\[Phi][]^2/(2 Sqrt[Deth[]])-(\[Kappa] \[Pi]a[]^2)/(12 Deth[]^(1/6))+(Sqrt[Deth[]] \[Sigma]2[]^2)/(2 \[Kappa])+(Sqrt[Deth[]] \[Sigma]3[]^2)/(2 \[Kappa])+(Sqrt[Deth[]] \[Sigma]4[]^2)/(2 \[Kappa])+(Sqrt[Deth[]] \[Sigma]5[]^2)/(2 \[Kappa])+(Sqrt[Deth[]] \[Sigma]6[]^2)/(2 \[Kappa]))}];*)


(* ::Subsection:: *)
(*Mode decomposition of diffeomorphism constraint*)


(* ::Input:: *)
(*diffeoa/.MakeRule[{PD[-b]@\[Delta]h[LI[1],-a,-c],I*kv[-b]\[Delta]h[LI[1],-a,-c]}]/.MakeRule[{PD[-d]@\[Delta]P[LI[1],a,b],I*kv[-d]\[Delta]P[LI[1],a,b]}]/.MakeRule[{PD[-i]@\[Delta]\[Phi][LI[1]],I*kv[-i]\[Delta]\[Phi][LI[1]]}]/.moderule1/.moderule2/.\[Gamma]0rule/.\[Pi]0rule//ContractMetric*)
(*(*Fourier transform the first-order diffeomorphism constraint*)*)


(* ::Input:: *)
(*diffeob=%/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org;*)
(*(*Decompose the shear and simplify *)*)


(* ::Input:: *)
(*(*Eqn. A12*)*)
(*diffeob1=kv[i]diffeob//ToCanonical*)
(*(*Component of diffeomorphism constraint along kv.*)*)


(* ::Input:: *)
(*(*Eqn. A13*)*)
(*diffeob2=e1[i]diffeob//ToCanonical*)
(*(*Component of diffeomorphism constraint along e1.*)*)


(* ::Input:: *)
(*(*Eqn. A14*)*)
(*diffeob3=e2[i]diffeob//ToCanonical*)
(*(*Component of diffeomorphism constraint along e2.*)*)


(* ::Subsection:: *)
(*Mode decomposition of Hamiltonian constraint*)


(* ::Input:: *)
(*S1b*)


(* ::Input:: *)
(*S1b/.MakeRule[{PD[-b]@\[Delta]h[LI[1],-a,-c],I*kv[-b]\[Delta]h[LI[1],-a,-c]}]/.MakeRule[{PD[-b]@\[Delta]h[LI[1],-a,-c],I*kv[-b]\[Delta]h[LI[1],-a,-c]}]/.MakeRule[{PD[-d]@kv[-b],0}](*/.MakeRule[{g[c,d]kv[-c]kv[-d],kk[]^2}]*)//ContractMetric*)
(*(*Fourier transform the first-order Hamiltonian constraint*)*)


(* ::Input:: *)
(*%/.moderule1/.moderule2/.\[Gamma]0rule/.\[Pi]0rule//ToCanonical*)
(*(*Decompose in to SVT modes*)*)


(* ::Input:: *)
(*(*Eqn. A11*)*)
(*S1c=%/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule/.\[Sigma]brule//org*)
(*(*Decompose the shear and simplify*)*)


(* ::Input:: *)
(*(*Below, we verify that the constraints commute with each other modulo background constraints.*)*)


(* ::Input:: *)
(*PoissonBracket[S1c,diffeob1,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//org*)
(*%/.bgconstraintrule//org*)


(* ::Input:: *)
(*PoissonBracket[S1c,diffeob2,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//org*)


(* ::Input:: *)
(*PoissonBracket[S1c,diffeob3,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//org*)


(* ::Input:: *)
(*PoissonBracket[diffeob2,diffeob1,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//org*)


(* ::Input:: *)
(*PoissonBracket[diffeob3,diffeob1,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//org*)


(* ::Input:: *)
(*PoissonBracket[diffeob2,diffeob3,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//org*)


(* ::Text:: *)
(* *)


(* ::Section:: *)
(*Construction of gauge invariant variables*)


(* ::Text:: *)
(*The perturbed phase space is fourteen dimensional. However, there are four first - order constraints. So it is essential to identify the remaining 6 true degrees of freedom. We will try to identify part of the true degree of freedom by inspection.*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]0[],S1c,{\[Delta]\[Phi][LI[1]],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]1[],S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]2[],S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]3[],S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]4[],S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]5[],S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]6[],S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Delta]\[Phi][LI[1]],diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]1[],diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]2[],diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A2rule*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]3[],diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A3rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]4[],diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A4rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]5[],diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]6[],diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]*)


(* ::Input:: *)
(*(*From above commutation relations of variables,we can deduce that the following combination of variables commutes with the constraints:*)*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]0[]+x(Sqrt[2]\[Gamma]1[]- \[Gamma]2[]),S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A2rule/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*Solve[%==0,x]*)


(* ::Input:: *)
(*(*Eqn. 3.14*)*)
(*\[CapitalGamma]0new=(*Deth[]^(1/6)*)(\[Gamma]0[]+(6 Sqrt[\[Kappa]] P\[Phi][])/(Deth[]^(1/6) (Sqrt[6] \[Kappa] \[Pi]a[]+6 Deth[]^(1/3) \[Sigma]2[])) (Sqrt[2]\[Gamma]1[]-\[Gamma]2[]))*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]5[]+x(Sqrt[2] \[Gamma]1[]- \[Gamma]2[]),S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A2rule/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*Solve[%==0,x]*)


(* ::Input:: *)
(*(*Eqn. 3.15*)*)
(*\[CapitalGamma]1new=(*Detg[]^(1/6)(\[Gamma]5[]-Detg[]^(1/3)/P\[CurlyPhi][]2\[Sigma]Tp[]\[Delta]\[CurlyPhi][LI[1]]);*)(*Deth[]^(1/6)/(2Sqrt[\[Kappa]])*)(\[Gamma]5[]+(6 Deth[]^(1/3) \[Sigma]5[])/(Sqrt[6] \[Kappa] \[Pi]a[]+6 Deth[]^(1/3) \[Sigma]2[]) (Sqrt[2]\[Gamma]1[]-\[Gamma]2[]))*)


(* ::Input:: *)
(*PoissonBracket[\[Gamma]6[]+x( Sqrt[2]\[Gamma]1[]- \[Gamma]2[]),S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A2rule/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*Solve[%==0,x]*)


(* ::Input:: *)
(*(*Eqn. 3.16*)*)
(*\[CapitalGamma]2new=(*Detg[]^(1/6)(\[Gamma]6[]-Detg[]^(1/3)/P\[CurlyPhi][]2\[Sigma]Tc[]\[Delta]\[CurlyPhi][LI[1]]);*)(*Deth[]^(1/6)/(2Sqrt[\[Kappa]])*)(\[Gamma]6[]+(6 Deth[]^(1/3) \[Sigma]6[])/(Sqrt[6] \[Kappa] \[Pi]a[]+6 Deth[]^(1/3) \[Sigma]2[]) (Sqrt[2]\[Gamma]1[]-\[Gamma]2[]))*)


(* ::Subsubsection:: *)
(*Check that the new variables are indeed gauge independent*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new,diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A2rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new,S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//Expand//org//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new,diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.A2rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new,S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//Expand//org//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new,diffeob,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new,S1c,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org//Simplify*)


(* ::Section:: *)
(*Canonical transformation to gauge invariant variables plus pure gauge*)


(* ::Text:: *)
(*By inspection, we have found a part of the gauge invariant variables. We would like to construct the rest of the phase space. We would also like to implement the whole transformation from the fourier modes to the new variables as a canonical transformation. In order to achieve this, we construct a generating function with the pure gauge momenta set equal to the four constraints.*)


(* ::Subsection:: *)
(*Generating function*)


(* ::Input:: *)
(*(*Let us define the new variables and momenta*)*)


(* ::Input:: *)
(*DefTensor[\[CapitalGamma]0[],{M3,t}]*)
(*DefTensor[\[CapitalGamma]1[],{M3,t}]*)
(*DefTensor[\[CapitalGamma]2[],{M3,t}]*)
(*DefTensor[\[CapitalGamma]3[],{M3,t}]*)
(*DefTensor[\[CapitalGamma]4[],{M3,t}]*)
(*DefTensor[\[CapitalGamma]5[],{M3,t}]*)
(*DefTensor[\[CapitalGamma]6[],{M3,t}]*)
(*DefTensor[\[CapitalPi]0[],{M3,t}]*)
(*DefTensor[\[CapitalPi]1[],{M3,t}]*)
(*DefTensor[\[CapitalPi]2[],{M3,t}]*)
(*DefTensor[\[CapitalPi]3[],{M3,t}]*)
(*DefTensor[\[CapitalPi]4[],{M3,t}]*)
(*DefTensor[\[CapitalPi]5[],{M3,t}]*)
(*DefTensor[\[CapitalPi]6[],{M3,t}]*)


(* ::Input:: *)
(*(*Now we define,many coefficients that will appear in the generating function.*)*)


(* ::Input:: *)
(*DefTensor[A00[],{M3,t}]*)
(*DefTensor[A01[],{M3,t}]*)
(*DefTensor[A02[],{M3,t}]*)
(*DefTensor[A03[],{M3,t}]*)
(*DefTensor[A04[],{M3,t}]*)
(*DefTensor[A05[],{M3,t}]*)
(*DefTensor[A06[],{M3,t}]*)
(*DefTensor[A11[],{M3,t}]*)
(*DefTensor[A12[],{M3,t}]*)
(*DefTensor[A13[],{M3,t}]*)
(*DefTensor[A14[],{M3,t}]*)
(*DefTensor[A15[],{M3,t}]*)
(*DefTensor[A16[],{M3,t}]*)
(*DefTensor[A22[],{M3,t}]*)
(*DefTensor[A23[],{M3,t}]*)
(*DefTensor[A24[],{M3,t}]*)
(*DefTensor[A25[],{M3,t}]*)
(*DefTensor[A26[],{M3,t}]*)
(*DefTensor[A33[],{M3,t}]*)
(*DefTensor[A34[],{M3,t}]*)
(*DefTensor[A35[],{M3,t}]*)
(*DefTensor[A36[],{M3,t}]*)
(*DefTensor[A44[],{M3,t}]*)
(*DefTensor[A45[],{M3,t}]*)
(*DefTensor[A46[],{M3,t}]*)
(*DefTensor[A55[],{M3,t}]*)
(*DefTensor[A56[],{M3,t}]*)
(*DefTensor[A66[],{M3,t}]*)


(* ::Input:: *)
(*DefTensor[C0[],{M3,t}]*)
(*DefTensor[C1[],{M3,t}]*)
(*DefTensor[C2[],{M3,t}]*)
(*DefTensor[C3[],{M3,t}]*)
(*DefTensor[C4[],{M3,t}]*)
(*DefTensor[C5[],{M3,t}]*)
(*DefTensor[C6[],{M3,t}]*)
(*DefTensor[E0[],{M3,t}]*)
(*DefTensor[E1[],{M3,t}]*)
(*DefTensor[E2[],{M3,t}]*)
(*DefTensor[E3[],{M3,t}]*)
(*DefTensor[E4[],{M3,t}]*)
(*DefTensor[E5[],{M3,t}]*)
(*DefTensor[E6[],{M3,t}]*)


(* ::Input:: *)
(*DefTensor[F0[],{M3,t}]*)
(*DefTensor[F1[],{M3,t}]*)
(*DefTensor[F2[],{M3,t}]*)
(*DefTensor[F3[],{M3,t}]*)
(*DefTensor[F4[],{M3,t}]*)
(*DefTensor[F5[],{M3,t}]*)
(*DefTensor[F6[],{M3,t}]*)
(*DefTensor[J0[],{M3,t}]*)
(*DefTensor[J1[],{M3,t}]*)
(*DefTensor[J2[],{M3,t}]*)
(*DefTensor[J3[],{M3,t}]*)
(*DefTensor[J4[],{M3,t}]*)
(*DefTensor[J5[],{M3,t}]*)
(*DefTensor[J6[],{M3,t}]*)


(* ::Input:: *)
(*(*We construct the new pure gauge configuration variables as a linear combination *)
(*of all the old variables.*)*)
(*\[CapitalGamma]3new=C0[] \[Gamma]0[](*\[Delta]\[Phi][LI[1]]*)+C1[]\[Gamma]1[]+C2[]\[Gamma]2[]+C3[]\[Gamma]3[]+C4[]\[Gamma]4[]+C5[]\[Gamma]5[]+C6[]\[Gamma]6[];*)
(*\[CapitalGamma]4new=E0[] \[Gamma]0[]+E1[]\[Gamma]1[]+E2[]\[Gamma]2[]+E3[]\[Gamma]3[]+E4[]\[Gamma]4[]+E5[]\[Gamma]5[]+E6[]\[Gamma]6[];*)
(*\[CapitalGamma]5new=F0[] \[Gamma]0[]+F1[]\[Gamma]1[]+F2[]\[Gamma]2[]+F3[]\[Gamma]3[]+F4[]\[Gamma]4[]+F5[]\[Gamma]5[]+F6[]\[Gamma]6[];*)
(*\[CapitalGamma]6new=J0[] \[Gamma]0[]+J1[]\[Gamma]1[]+J2[]\[Gamma]2[]+J3[]\[Gamma]3[]+J4[]\[Gamma]4[]+J5[]\[Gamma]5[]+J6[]\[Gamma]6[];*)


(* ::Input:: *)
(*(*The generating function depends on the old variable and the new momenta,G (Subscript[\[Gamma],i],P^i).*)*)
(*G=\[CapitalGamma]0new \[CapitalPi]0[]+\[CapitalGamma]1new \[CapitalPi]1[]+ \[CapitalGamma]2new \[CapitalPi]2[]+A00[]\[Gamma]0[]\[Gamma]0[]+2A01[]\[Gamma]0[]\[Gamma]1[]+2A02[]\[Gamma]0[]\[Gamma]2[]+2A03[]\[Gamma]0[]\[Gamma]3[]+2A04[]\[Gamma]0[]\[Gamma]4[]+2A05[]\[Gamma]0[]\[Gamma]5[]+2A06[]\[Gamma]0[]\[Gamma]6[]+A11[]\[Gamma]1[]\[Gamma]1[]+2A12[]\[Gamma]1[]\[Gamma]2[]+2A13[]\[Gamma]1[]\[Gamma]3[]+2A14[]\[Gamma]1[]\[Gamma]4[]+2A15[]\[Gamma]1[]\[Gamma]5[]+2A16[]\[Gamma]1[]\[Gamma]6[]+A22[]\[Gamma]2[]\[Gamma]2[]+2A23[]\[Gamma]2[]\[Gamma]3[]+2A24[]\[Gamma]2[]\[Gamma]4[]+2A25[]\[Gamma]2[]\[Gamma]5[]+2A26[]\[Gamma]2[]\[Gamma]6[]+A33[]\[Gamma]3[]\[Gamma]3[]+2A34[]\[Gamma]3[]\[Gamma]4[]+2A35[]\[Gamma]3[]\[Gamma]5[]+2A36[]\[Gamma]3[]\[Gamma]6[]+A44[]\[Gamma]4[]\[Gamma]4[]+2A45[]\[Gamma]4[]\[Gamma]5[]+2A46[]\[Gamma]4[]\[Gamma]6[]+A55[]\[Gamma]5[]\[Gamma]5[]+2A56[]\[Gamma]5[]\[Gamma]6[]+A66[]\[Gamma]6[]\[Gamma]6[]+\[CapitalGamma]3new \[CapitalPi]3[]+\[CapitalGamma]4new \[CapitalPi]4[]+\[CapitalGamma]5new \[CapitalPi]5[]+\[CapitalGamma]6new \[CapitalPi]6[]//NoScalar*)


(* ::Input:: *)
(*(*Once G is known,we can calculate the old momenta in terms of new momenta and old configuration variables as:*)*)
(*\[Pi]0old(*\[Delta]P\[Phi]old*)=D[G,\[Gamma]0[]]*)
(*\[Pi]1old=D[G,\[Gamma]1[]]*)
(*\[Pi]2old=D[G,\[Gamma]2[]]*)
(*\[Pi]3old=D[G,\[Gamma]3[]]*)
(*\[Pi]4old=D[G,\[Gamma]4[]]*)
(*\[Pi]5old=D[G,\[Gamma]5[]]*)
(*\[Pi]6old=D[G,\[Gamma]6[]]*)


(* ::Input:: *)
(*(*Our aim is to set some of the new momenta equal to the constraints. Hence,we will rewrite the constraints in terms of the old variables and the new momenta. If we want to set the constraints equal to a new momenta,say \[CapitalPi]3,then we can demand that the coefficients of all the configuration variables and the new momenta except the concerned momenta,\[CapitalPi]3 in this case,to be zero. Furthermore,the coefficient of the concerned momenta,in this case \[CapitalPi]3,should be set to 1. This leads to a set of equations of the coefficents appearing in the generating function.*)*)


(* ::Input:: *)
(*S1d=(S1c/k[])/.MakeRule[{\[Pi]0[],\[Pi]0old}]/.MakeRule[{\[Pi]1[],\[Pi]1old}]/.MakeRule[{\[Pi]2[],\[Pi]2old}]/.MakeRule[{\[Pi]3[],\[Pi]3old}]/.MakeRule[{\[Pi]4[],\[Pi]4old}]/.MakeRule[{\[Pi]5[],\[Pi]5old}]/.MakeRule[{\[Pi]6[],\[Pi]6old}]//org*)
(*(*First-order Hamiltonian constraint in terms of the old variables and the new momenta.*)*)


(* ::Input:: *)
(*diffeoc1=(diffeob1/I/k[])/.MakeRule[{\[Pi]0[],\[Pi]0old}]/.MakeRule[{\[Pi]1[],\[Pi]1old}]/.MakeRule[{\[Pi]2[],\[Pi]2old}]/.MakeRule[{\[Pi]3[],\[Pi]3old}]/.MakeRule[{\[Pi]4[],\[Pi]4old}]/.MakeRule[{\[Pi]5[],\[Pi]5old}]/.MakeRule[{\[Pi]6[],\[Pi]6old}]//org*)
(*(*First-order diffeomorphism constraint,along kv,in terms of the old variables and the new momenta.*)*)


(* ::Input:: *)
(*diffeoc2=(diffeob2/I/k[])/.MakeRule[{\[Pi]0[],\[Pi]0old}]/.MakeRule[{\[Pi]1[],\[Pi]1old}]/.MakeRule[{\[Pi]2[],\[Pi]2old}]/.MakeRule[{\[Pi]3[],\[Pi]3old}]/.MakeRule[{\[Pi]4[],\[Pi]4old}]/.MakeRule[{\[Pi]5[],\[Pi]5old}]/.MakeRule[{\[Pi]6[],\[Pi]6old}]//org*)
(*(*First-order diffeomorphism constraint,along e1,in terms of the old variables and the new momenta.*)*)


(* ::Input:: *)
(*diffeoc3=(diffeob3/I/k[])/.MakeRule[{\[Pi]0[],\[Pi]0old}]/.MakeRule[{\[Pi]1[],\[Pi]1old}]/.MakeRule[{\[Pi]2[],\[Pi]2old}]/.MakeRule[{\[Pi]3[],\[Pi]3old}]/.MakeRule[{\[Pi]4[],\[Pi]4old}]/.MakeRule[{\[Pi]5[],\[Pi]5old}]/.MakeRule[{\[Pi]6[],\[Pi]6old}]//org*)
(*(*First-order diffeomorphism constraint,along e2,in terms of the old variables and the new momenta.*)*)


(* ::Subsubsection:: *)
(*Equations for coefficients*)


(* ::Input:: *)
(*eq1=Coefficient[S1d,\[Gamma]0[]]*)


(* ::Input:: *)
(*eq2=Coefficient[S1d,\[Gamma]1[]]*)


(* ::Input:: *)
(*eq3=Coefficient[S1d,\[Gamma]2[]]*)


(* ::Input:: *)
(*eq4=Coefficient[S1d,\[Gamma]3[]]*)


(* ::Input:: *)
(*eq5=Coefficient[S1d,\[Gamma]4[]]*)


(* ::Input:: *)
(*eq6=Coefficient[S1d,\[Gamma]5[]]*)


(* ::Input:: *)
(*eq7=Coefficient[S1d,\[Gamma]6[]]*)


(* ::Input:: *)
(*eq8=Coefficient[S1d,\[CapitalPi]0[]]//Simplify*)


(* ::Input:: *)
(*eq9=Coefficient[S1d,\[CapitalPi]3[]]*)


(* ::Input:: *)
(*eq10=Coefficient[S1d,\[CapitalPi]4[]]*)


(* ::Input:: *)
(*eq11=Coefficient[S1d,\[CapitalPi]5[]]*)


(* ::Input:: *)
(*eq12=Coefficient[S1d,\[CapitalPi]6[]]*)


(* ::Input:: *)
(*eq13=Coefficient[S1d,\[CapitalPi]1[]]//Simplify*)


(* ::Input:: *)
(*eq14=Coefficient[S1d,\[CapitalPi]2[]]//Simplify*)


(* ::Input:: *)
(*eq1a=Coefficient[diffeoc1,\[Gamma]0[]]*)


(* ::Input:: *)
(*eq2a=Coefficient[diffeoc1,\[Gamma]1[]]*)


(* ::Input:: *)
(*eq3a=Coefficient[diffeoc1,\[Gamma]2[]]*)


(* ::Input:: *)
(*eq4a=Coefficient[diffeoc1,\[Gamma]3[]]*)


(* ::Input:: *)
(*eq5a=Coefficient[diffeoc1,\[Gamma]4[]]*)


(* ::Input:: *)
(*eq6a=Coefficient[diffeoc1,\[Gamma]5[]]*)


(* ::Input:: *)
(*eq7a=Coefficient[diffeoc1,\[Gamma]6[]]*)


(* ::Input:: *)
(*eq8a=Coefficient[diffeoc1,\[CapitalPi]0[]]*)


(* ::Input:: *)
(*eq9a=Coefficient[diffeoc1,\[CapitalPi]3[]]*)


(* ::Input:: *)
(*eq10a=Coefficient[diffeoc1,\[CapitalPi]4[]]*)


(* ::Input:: *)
(*eq11a=Coefficient[diffeoc1,\[CapitalPi]5[]]*)


(* ::Input:: *)
(*eq12a=Coefficient[diffeoc1,\[CapitalPi]6[]]*)


(* ::Input:: *)
(*eq13a=Coefficient[diffeoc1,\[CapitalPi]1[]]*)


(* ::Input:: *)
(*eq14a=Coefficient[diffeoc1,\[CapitalPi]2[]]*)


(* ::Input:: *)
(*eq1b=Coefficient[diffeoc2,\[Gamma]0[]]*)


(* ::Input:: *)
(*eq2b=Coefficient[diffeoc2,\[Gamma]1[]]*)


(* ::Input:: *)
(*eq3b=Coefficient[diffeoc2,\[Gamma]2[]]*)


(* ::Input:: *)
(*eq4b=Coefficient[diffeoc2,\[Gamma]3[]]*)


(* ::Input:: *)
(*eq5b=Coefficient[diffeoc2,\[Gamma]4[]]*)


(* ::Input:: *)
(*eq6b=Coefficient[diffeoc2,\[Gamma]5[]]*)


(* ::Input:: *)
(*eq7b=Coefficient[diffeoc2,\[Gamma]6[]]*)


(* ::Input:: *)
(*eq8b=Coefficient[diffeoc2,\[CapitalPi]0[]]*)


(* ::Input:: *)
(*eq9b=Coefficient[diffeoc2,\[CapitalPi]3[]]*)


(* ::Input:: *)
(*eq10b=Coefficient[diffeoc2,\[CapitalPi]4[]]*)


(* ::Input:: *)
(*eq11b=Coefficient[diffeoc2,\[CapitalPi]5[]]*)


(* ::Input:: *)
(*eq12b=Coefficient[diffeoc2,\[CapitalPi]6[]]*)


(* ::Input:: *)
(*eq13b=Coefficient[diffeoc2,\[CapitalPi]1[]]*)


(* ::Input:: *)
(*eq14b=Coefficient[diffeoc2,\[CapitalPi]2[]]*)


(* ::Input:: *)
(*eq1c=Coefficient[diffeoc3,\[Gamma]0[]]*)


(* ::Input:: *)
(*eq2c=Coefficient[diffeoc3,\[Gamma]1[]]*)


(* ::Input:: *)
(*eq3c=Coefficient[diffeoc3,\[Gamma]2[]]*)


(* ::Input:: *)
(*eq4c=Coefficient[diffeoc3,\[Gamma]3[]]*)


(* ::Input:: *)
(*eq5c=Coefficient[diffeoc3,\[Gamma]4[]]*)


(* ::Input:: *)
(*eq6c=Coefficient[diffeoc3,\[Gamma]5[]]*)


(* ::Input:: *)
(*eq7c=Coefficient[diffeoc3,\[Gamma]6[]]*)


(* ::Input:: *)
(*eq8c=Coefficient[diffeoc3,\[CapitalPi]0[]]*)


(* ::Input:: *)
(*eq9c=Coefficient[diffeoc3,\[CapitalPi]3[]]*)


(* ::Input:: *)
(*eq10c=Coefficient[diffeoc3,\[CapitalPi]4[]]*)


(* ::Input:: *)
(*eq11c=Coefficient[diffeoc3,\[CapitalPi]5[]]*)


(* ::Input:: *)
(*eq12c=Coefficient[diffeoc3,\[CapitalPi]6[]]*)


(* ::Input:: *)
(*eq13c=Coefficient[diffeoc3,\[CapitalPi]1[]]*)


(* ::Input:: *)
(*eq14c=Coefficient[diffeoc3,\[CapitalPi]2[]]*)


(* ::Input:: *)
(*(*Of the 56 coefficients,only a subset of them can be solved.*)*)


(* ::Input:: *)
(*sAij=Solve[{eq1c==0,eq2c==0,eq3c==0,eq4c==0,eq5c==0,eq6c==0,eq7c==0,eq1b==0,eq2b==0,eq3b==0,eq4b==0,eq5b==0,eq6b==0,eq7b==0,eq2a==0,eq3a==0,eq1a==0,eq2==0,eq6a==0,eq3==0,eq1==0,eq6==0,eq7==0,eq4==0,eq5==0,eq4a==0,eq5a==0(*,eq7a\[Equal]0*)},{A04[],A14[],A24[],A34[],A44[],A45[],A46[],A03[],A13[],A23[],A33[],A35[],A36[],A11[],A22[],A02[],A16[],A15[],A25[],A00[],A55[],A66[]}]*)
(*(*Solving for coefficients Aij. Of the 28 coefficients,only 22 has been solved.The remaining of them appear as free parameters.*)*)


(* ::Text:: *)
(*eq7a has not been included in the above system, because the system was not solvable (it should be if one includes the decomposition of \[Sigma]b and the background constraint). In what follows, I verify that eq7a is satisfied by the solutions sAij, if I use the background constraints and the decomposition of \[Sigma]b[]^2:*)


(* ::Input:: *)
(*\[Sigma]6[](2eq7a)/(I k[]^2)/.Flatten[sAij]/.bgconstraintrule/.MakeRule[{\[Sigma]b[]^2,sigmasq}]//ToCanonical*)


(* ::Input:: *)
(*sCEFJ=Solve[{eq9c==0,eq10c==0,eq11c==0,eq12c==1,eq9b==0,eq10b==0,eq11b==1,eq12b==0,eq9==1,eq9a==0,eq10==0,eq10a==1,eq11==0,eq11a==0,eq12==0,eq12a==0},{C4[],E4[],F4[],J4[],C3[],E3[],F3[],J3[],C1[],C2[],E1[],E2[],F1[],F2[],J1[],J2[]}]*)
(*(*Only 16 of the equations can be solved,leaving 12 free parameters.*)*)


(* ::Input:: *)
(*(*Thus,the generating function can now be written as follows. This is a function of old variables,new momenta and many (18) free parameters.*)*)
(*G1=G/.Flatten[sAij]/.Flatten[sCEFJ]/.bgconstraintrule//NoScalar*)


(* ::Subsubsection:: *)
(*Is part of the new momenta indeed the constraints?*)


(* ::Input:: *)
(*(*To check this,first calculate the old momenta from the generating function*)*)


(* ::Input:: *)
(*\[Pi]0old1=D[G1,\[Gamma]0[]](*/.bgconstraintrule*)//org*)


(* ::Input:: *)
(*\[Pi]1old1=D[G1,\[Gamma]1[]]//org;*)


(* ::Input:: *)
(*\[Pi]2old1=D[G1,\[Gamma]2[]]//org;*)


(* ::Input:: *)
(*\[Pi]3old1=D[G1,\[Gamma]3[]]//org;*)


(* ::Input:: *)
(*\[Pi]4old1=D[G1,\[Gamma]4[]]//org;*)


(* ::Input:: *)
(*\[Pi]5old1=D[G1,\[Gamma]5[]]//org;*)


(* ::Input:: *)
(*\[Pi]6old1=D[G1,\[Gamma]6[]]//org;*)


(* ::Input:: *)
(*(*Now,invert the expression for old momenta in terms of new momenta and old configuration variables to find the new momenta.*)*)


(* ::Input:: *)
(*spn=Solve[{\[Pi]0old1==\[Pi]0[],\[Pi]1old1==\[Pi]1[],\[Pi]2old1==\[Pi]2[],\[Pi]3old1==\[Pi]3[],\[Pi]4old1==\[Pi]4[],\[Pi]5old1==\[Pi]5[],\[Pi]6old1==\[Pi]6[]},{\[CapitalPi]0[],\[CapitalPi]3[],\[CapitalPi]4[],\[CapitalPi]5[],\[CapitalPi]6[],\[CapitalPi]1[],\[CapitalPi]2[]}]//Simplify;*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*\[CapitalPi]0new1=\[CapitalPi]0[]/.Flatten[spn];*)


(* ::Input:: *)
(*\[CapitalPi]3new1=\[CapitalPi]3[]/.Flatten[spn](*//Simplify*);*)


(* ::Input:: *)
(*\[CapitalPi]4new1=\[CapitalPi]4[]/.Flatten[spn](*//Simplify*);*)


(* ::Input:: *)
(*\[CapitalPi]5new1=\[CapitalPi]5[]/.Flatten[spn];*)


(* ::Input:: *)
(*\[CapitalPi]6new1=\[CapitalPi]6[]/.Flatten[spn];*)


(* ::Input:: *)
(*\[CapitalPi]1new1=\[CapitalPi]1[]/.Flatten[spn](*//Simplify*);*)


(* ::Input:: *)
(*\[CapitalPi]2new1=\[CapitalPi]2[]/.Flatten[spn](*//Simplify*);*)


(* ::Input:: *)
(*(*Now,one can compare the expressions for the new momenta \[CapitalPi]3,\[CapitalPi]4,\[CapitalPi]5 and \[CapitalPi]6 with the constraints.*)*)


(* ::Input:: *)
(*(\[CapitalPi]3new1-S1c/k[])//Simplify*)
(*%/.bgconstraintrule/.\[Sigma]brule//org*)


(* ::Input:: *)
(*Simplify[\[CapitalPi]4new1-diffeob1/I/k[]]*)


(* ::Input:: *)
(*\[CapitalPi]5new1-diffeob2/I/k[]//ToCanonical*)


(* ::Input:: *)
(*\[CapitalPi]6new1-diffeob3/I/k[]//ToCanonical*)


(* ::Subsubsection:: *)
(*Are the new configuration variables and momenta conjugate to each other?*)


(* ::Input:: *)
(*(*To find this out,first we have to compute the new configuration variables*)*)


(* ::Input:: *)
(*\[CapitalGamma]0new1=D[G1,\[CapitalPi]0[]]//Simplify;*)


(* ::Input:: *)
(*\[CapitalGamma]3new1=D[G1,\[CapitalPi]3[]]//Simplify;*)


(* ::Input:: *)
(*\[CapitalGamma]4new1=D[G1,\[CapitalPi]4[]]//Simplify;*)


(* ::Input:: *)
(*\[CapitalGamma]5new1=D[G1,\[CapitalPi]5[]]//Simplify;*)


(* ::Input:: *)
(*\[CapitalGamma]6new1=D[G1,\[CapitalPi]6[]]//Simplify;*)


(* ::Input:: *)
(*\[CapitalGamma]1new1=D[G1,\[CapitalPi]1[]]//Simplify;*)


(* ::Input:: *)
(*\[CapitalGamma]2new1=D[G1,\[CapitalPi]2[]]//Simplify;*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*(*Eq. B8*)*)
(*(*Now,one can check the commutation relations:*)*)


(* ::Input:: *)
(*Q={\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]};*)
(*PQ={\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]};*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]0new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalGamma]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalGamma]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalGamma]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalGamma]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalGamma]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalGamma]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]0new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalGamma]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalGamma]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalGamma]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalGamma]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalGamma]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]0new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]3new1,\[CapitalPi]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalGamma]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalGamma]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalGamma]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalGamma]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalPi]0new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalPi]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalPi]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalPi]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalPi]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]4new1,\[CapitalPi]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalGamma]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalGamma]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]5new1,\[CapitalGamma]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalPi]0new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalPi]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalPi]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalPi]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalPi]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalPi]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalPi]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalGamma]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]6new1,\[CapitalGamma]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]0new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalGamma]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]0new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]3new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]4new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]5new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]6new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]1new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]2new1,Q,PQ]//Simplify*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Subsubsection:: *)
(*Replacement rule for old variables*)


(* ::Input:: *)
(*(*Having ensured that the generating function is correct,we now construct the expressions for old configuration variables in terms of new configuration variables and momenta.*)*)


(* ::Input:: *)
(*sov=org[Solve[{\[CapitalGamma]0new==\[CapitalGamma]0[],\[CapitalGamma]3new1==\[CapitalGamma]3[],\[CapitalGamma]4new1==\[CapitalGamma]4[],\[CapitalGamma]5new1==\[CapitalGamma]5[],\[CapitalGamma]6new1==\[CapitalGamma]6[],\[CapitalGamma]1new1==\[CapitalGamma]1[],\[CapitalGamma]2new1==\[CapitalGamma]2[]},{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]}]]//FullSimplify;*)


(* ::Input:: *)
(*\[Gamma]3[]/.Flatten[sov]//Expand//FullSimplify*)


(* ::Section:: *)
(*Second order Hamiltonian*)


(* ::Subsection:: *)
(*Replacing the old variables by new gauge independent and pure gauge variables*)


(* ::Input:: *)
(*S2a=(* Deth[]^(1/6)*)SeriesCoefficient[S,2](*//org*);*)
(*(*Second-order Hamiltonian for generic spacetime.*)*)


(* ::Input:: *)
(*S2a/.MakeRule[{PD[-a]@PD[-b]@\[Delta]h[LI[1],-c,-d],-kv[-a] kv[-b] \[Delta]h[LI[1],-c,-d]}]/.MakeRule[{PD[-a]@\[Delta]h[LI[1],-b,-c] PD[-d]@\[Delta]h[LI[1],-e,-f],kv[-a] kv[-d] \[Delta]h[LI[1],-b,-c] \[Delta]h[LI[1],-e,-f]}]/.MakeRule[{PD[-a]@\[Delta]\[Phi][LI[1]] PD[-b]@\[Delta]\[Phi][LI[1]],kv[-a] kv[-b] \[Delta]\[Phi][LI[1]] \[Delta]\[Phi][LI[1]]}]/.MakeRule[{h[a,b] kv[-a] kv[-b],k[]^2}]/.MakeRule[{PD[-c]@PD[-d]@\[Delta]h[LI[2],-a,-b],0}]/.MakeRule[{PD[-b]@PD[-d]@\[Delta]h[LI[2],-a,-c],0}]/.bgmomrule//org;(*//NoScalar*)*)
(*(*Fourier transform*)*)


(* ::Input:: *)
(*%/.moderule1/.moderule2/.\[Gamma]0rule/.\[Pi]0rule//org;*)
(*(*Decompose the metric perturbation and its conjugate momentum in to SVT modes.*)*)


(* ::Input:: *)
(*%/.sheardecomposition//org;*)
(*(*Decompose the shear.*)*)


(* ::Input:: *)
(*(*Eqn. A16*)*)
(*S2b=%/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule/.\[Sigma]brule//org;*)
(*(*Simplify the expressions.*)*)


(* ::Input:: *)
(*(*One can verify the Eqn. A16 as follows. Note that the complex nature of one of the mode has to be kept in mind.*)*)
(*Coefficient[S2b,\[Gamma]1[]\[Pi]1[]]*)


(* ::Input:: *)
(*Coefficient[S2b,\[Gamma]1[]\[Pi]2[]]*)


(* ::Input:: *)
(*Coefficient[S2b,\[Gamma]6[]\[Pi]6[]]*)


(* ::Input:: *)
(*S2c=S2b/.MakeRule[{\[Pi]0[],\[Pi]0old1}]/.MakeRule[{\[Pi]1[],\[Pi]1old1}]/.MakeRule[{\[Pi]2[],\[Pi]2old1}]/.MakeRule[{\[Pi]3[],\[Pi]3old1}]/.MakeRule[{\[Pi]4[],\[Pi]4old1}]/.MakeRule[{\[Pi]5[],\[Pi]5old1}]/.MakeRule[{\[Pi]6[],\[Pi]6old1}]/.Flatten[sov]//org;*)
(*(*Replace the old momenta in terms of new momenta and old variables and then replace the old variables in terms of new momenta and new configuration variables.*)*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Subsection:: *)
(*Derivative of first generating function*)


(* ::Text:: *)
(*To obtain the second - order Hamiltonian in terms of new variables, one needs to add the time evolution of the two generating functions (one used for transforming the phase - space variable from metric and its conjugate momenta to the SVT modes and the second for transforming from the SVT modes to the gauge independent plus the pure gauge variables).Firstly, we will focus on the first canonical transformation.*)


(* ::Input:: *)
(*(**)dhijdt=(4\[Kappa] (*Deth[]^(1/6)*))/Deth[]^(1/2) (P[a,b]h[-a,-i]h[-b,-j]-(h[-i,-j]P[a,b]h[-a,-b])/2)/.bgmomrule//ToCanonical*)


(* ::Input:: *)
(*dhijdtrule=MakeRule[{ParamD[t][h[-i,-j]],-((\[Kappa] h[-i, -j] \[Pi]a[])/(3 Deth[]^(1/3)))+2 \[Sigma][-i, -j]}]*)
(*(*Time derivative of Subscript[h, ij]*)*)


(* ::Input:: *)
(*dhIJdt=-((4\[Kappa] (*Deth[]^(1/6)*))/Deth[]^(1/2))(P[i,j]-(h[i,j]P[a,b]h[-a,-b])/2)/.bgmomrule//ToCanonical*)


(* ::Input:: *)
(*dhIJdtrule=MakeRule[{ParamD[t][h[i,j]],(\[Kappa] h[i, j] \[Pi]a[])/(3 Deth[]^(1/3))-2 \[Sigma][i, j]}]*)
(*(*Time derivative of h^ij*)*)


(* ::Input:: *)
(*(*dkiudt=kv[-i]/kk[]((kv[-a]kv[-b])/kk[]^2(2\[Kappa])/Detg[]^(1/2)(P[a,b]-g[a,b]P[-c,-d]g[c,d]/2))/.MakeRule[{P[a,b],Detg[]^(1/6)\[Pi]S[]/6g[a,b]+(Detg[]^(1/2)\[Sigma][a,b])/(2\[Kappa])}]//ToCanonical*)*)


(* ::Input:: *)
(*dkdt=1/(2k[]) kv[-i]kv[-j]dhIJdt//org*)


(* ::Input:: *)
(*AutomaticRules[k,MakeRule[{ParamD[t][k[]],dkdt}]]*)
(*(*Time derivative of k.*)*)


(* ::Input:: *)
(*dkIdt=dhIJdt kv[-j]*)


(* ::Input:: *)
(*dkIdtrule=MakeRule[{ParamD[t][kv[i]],kv[-j] ((\[Kappa] h[i, j] \[Pi]a[])/(3 Deth[]^(1/3))-2 \[Sigma][i, j])}]*)
(*(*Time derivative of kv^i*)*)


(* ::Input:: *)
(*dhijdt*)


(* ::Input:: *)
(*(*Note that Subscript[\[Sigma], ij]in this code is equal to a^2 *)*)


(* ::Input:: *)
(*R11=-Deth[]^(1/6)(*1*)e1[i]e1[j] dhijdt/2/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*R22=-Deth[]^(1/6)(*1*)e2[i]e2[j] dhijdt/2/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*R12=-Deth[]^(1/6)(*1*)e1[i]e2[j] dhijdt/2/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)


(* ::Input:: *)
(*de1Idt=Deth[]^(-1/6) (R11 e1[i] + R12 e2[i])*)


(* ::Input:: *)
(*de1Idtrule=MakeRule[{ParamD[t][e1[i]],(e1[i] ((\[Kappa] \[Pi]a[])/(6 Deth[]^(1/6))+(Deth[]^(1/6) \[Sigma]2[])/Sqrt[6]-(Deth[]^(1/6) \[Sigma]5[])/Sqrt[2])-(Deth[]^(1/6) e2[i] \[Sigma]6[])/Sqrt[2])/Deth[]^(1/6)}]*)
(*(*Time derivative of (e1^i).*)*)


(* ::Input:: *)
(*de1idt=ParamD[t][e1[j]h[-i,-j]]/.de1Idtrule/.dhijdtrule//ToCanonical*)
(*(*Time derivative of Subscript[e1, i].*)*)


(* ::Input:: *)
(*de1idtrule=MakeRule[{ParamD[t][e1[-i]],-((\[Kappa] e1[j] h[-i, -j] \[Pi]a[])/(6 Deth[]^(1/3)))+2 e1[j] \[Sigma][-i, -j]+(e1[j] h[-i, -j] \[Sigma]2[])/Sqrt[6]-(e1[j] h[-i, -j] \[Sigma]5[])/Sqrt[2]-(e2[j] h[-i, -j] \[Sigma]6[])/Sqrt[2]}]*)
(*(*Time derivative of Subscript[e1, i].*)*)


(* ::Input:: *)
(*de2Idt=Deth[]^(-1/6) (R12 e1[i] + R22 e2[i]) //ToCanonical*)


(* ::Input:: *)
(*de2Idtrule=MakeRule[{ParamD[t][e2[i]],(\[Kappa] e2[i] \[Pi]a[])/(6 Deth[]^(1/3))+(e2[i] \[Sigma]2[])/Sqrt[6]+(e2[i] \[Sigma]5[])/Sqrt[2]-(e1[i] \[Sigma]6[])/Sqrt[2]}]*)
(*(*Time derivative of (e2^i).*)*)


(* ::Input:: *)
(*de2idt=ParamD[t][e2[j]h[-i,-j]]/.de2Idtrule/.dhijdtrule//ToCanonical*)


(* ::Input:: *)
(*de2idtrule=MakeRule[{ParamD[t][e2[-i]],-((\[Kappa] e2[j] h[-i, -j] \[Pi]a[])/(6 Deth[]^(1/3)))+2 e2[j] \[Sigma][-i, -j]+(e2[j] h[-i, -j] \[Sigma]2[])/Sqrt[6]+(e2[j] h[-i, -j] \[Sigma]5[])/Sqrt[2]-(e1[j] h[-i, -j] \[Sigma]6[])/Sqrt[2]}]*)
(*(*Time derivative of Subscript[e2, i].*)*)


(* ::Input:: *)
(*dA2dt=ParamD[t][A2[-i,-j]/.A2rule]/.dhijdtrule/.MakeRule[{ParamD[t][kv[-i]],0}]/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)
(*(*Time derivative of Subscript[A2, ij].*)*)


(* ::Input:: *)
(*dA3dt=ParamD[t][A3[-i,-j]/.A3rule]/.MakeRule[{ParamD[t][kv[-i]],0}]/.de1idtrule/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)
(*(*Time derivative of Subscript[A3, ij].*)*)


(* ::Input:: *)
(*dA4dt=ParamD[t][A4[-i,-j]/.A4rule]/.MakeRule[{ParamD[t][kv[-i]],0}]/.de2idtrule/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)
(*(*Time derivative of Subscript[A4, ij].*)*)


(* ::Input:: *)
(*dA5dt=ParamD[t][A5[-i,-j]/.A5rule]/.de1idtrule/.de2idtrule/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)
(*(*Time derivative of Subscript[A5, ij].*)*)


(* ::Input:: *)
(*dA6dt=ParamD[t][A6[-i,-j]/.A6rule]/.de1idtrule/.de2idtrule/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)
(*(*Time derivative of Subscript[A6, ij].*)*)


(* ::Input:: *)
(*dG\[Gamma]dt=-\[Delta]P[LI[1],i,j](dhijdt \[Gamma]1[]/Sqrt[3]+dA2dt \[Gamma]2[]+dA3dt \[Gamma]3[]+dA4dt \[Gamma]4[]+dA5dt \[Gamma]5[]+dA6dt \[Gamma]6[])/.moderule2/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)
(*(*Time derivative of the first generating function.*)*)


(* ::Input:: *)
(*(*dSdt/.MakeRule[{\[Sigma]p[],0}]/.MakeRule[{\[Sigma]V1[],0}]/.MakeRule[{\[Sigma]V2[],0}]/.MakeRule[{\[Sigma]Tp[],0}]/.MakeRule[{\[Sigma]Tc[],0}]*)*)


(* ::Input:: *)
(*dG\[Gamma]dt1=dG\[Gamma]dt/.MakeRule[{\[Pi]0[],\[Pi]0old1}]/.MakeRule[{\[Pi]1[],\[Pi]1old1}]/.MakeRule[{\[Pi]2[],\[Pi]2old1}]/.MakeRule[{\[Pi]3[],\[Pi]3old1}]/.MakeRule[{\[Pi]4[],\[Pi]4old1}]/.MakeRule[{\[Pi]5[],\[Pi]5old1}]/.MakeRule[{\[Pi]6[],\[Pi]6old1}]/.Flatten[sov];*)
(*(*Time derivative of the first generating function in terms of new variables.*)*)


(* ::Subsection:: *)
(*Derivative of second generating function*)


(* ::Text:: *)
(*Now,let us calculate the background evolution of the second generating function.*)


(* ::Input:: *)
(*S0*)


(* ::Input:: *)
(*S0a*)


(* ::Input:: *)
(*PoissonBracket[\[Phi][],S0a,{\[Phi][]},{P\[Phi][]}]*)


(* ::Input:: *)
(*PoissonBracket[P\[Phi][],S0a,{\[Phi][]},{P\[Phi][]}]*)


(* ::Input:: *)
(*AutomaticRules[\[Phi],MakeRule[{ParamD[t][\[Phi][]],(*Deth[]^(1/6)*)P\[Phi][]/Sqrt[Deth[]]}]]*)
(*(*Time derivative of \[Phi]*)*)


(* ::Input:: *)
(*AutomaticRules[P\[Phi],MakeRule[{ParamD[t][P\[Phi][]],-(*Deth[]^(1/6)*)Sqrt[Deth[]] Derivative[1][V][\[Phi][]]}]]*)
(*(*Time derivative of P\[Phi].*)*)


(* ::Input:: *)
(*dPIJdt0= (*Deth[]^(1/6)*)(\[Kappa]/Deth[]^(1/2) h[i,j] h[-a,-c] h[-b,-d] P[a,b] P[c,d]-(4 \[Kappa])/Deth[]^(1/2) h[-b,-d] P[i,b] P[j,d]-\[Kappa]/(2 Deth[]^(1/2)) h[i,j] h[-a,-b] P[a,b] h[-c,-d] P[c,d]+(2 \[Kappa])/Deth[]^(1/2) h[-a,-b] P[a,b] P[i,j]+P\[Phi][]^2/(4 Deth[]^(1/2)) h[i,j]-Deth[]^(1/2)/2 h[i,j] V[\[Phi][]])/.bgmomrule//ContractMetric;*)
(*dPIJdt=dPIJdt0/.\[Sigma]brule//org*)


(* ::Input:: *)
(*dPIJdtrule=MakeRule[{ParamD[t][P[i,j]],(h[i, j] P\[Phi][]^2)/(4 Sqrt[Deth[]])-1/2 Sqrt[Deth[]] h[i, j] V[\[Phi][]]+(\[Kappa] h[i, j] \[Pi]a[]^2)/(72 Deth[]^(1/6))-1/6 Deth[]^(1/6) \[Pi]a[] \[Sigma][i, j]-(Sqrt[Deth[]] \[Sigma][i, b] \[Sigma][j, -b])/\[Kappa]+(Sqrt[Deth[]] h[i, j] \[Sigma]2[]^2)/(4 \[Kappa])+(Sqrt[Deth[]] h[i, j] \[Sigma]3[]^2)/(4 \[Kappa])+(Sqrt[Deth[]] h[i, j] \[Sigma]4[]^2)/(4 \[Kappa])+(Sqrt[Deth[]] h[i, j] \[Sigma]5[]^2)/(4 \[Kappa])+(Sqrt[Deth[]] h[i, j] \[Sigma]6[]^2)/(4 \[Kappa])}]*)
(*(*Time derivative of (P^ij).*)*)


(* ::Input:: *)
(*bgmomrule*)


(* ::Input:: *)
(*P[a,b]h[-a,-b]/.bgmomrule//org*)


(* ::Input:: *)
(*\[Pi]adot=ParamD[t][2 P[i,j]h[-i,-j]/Deth[]^(1/6)]/.dhijdtrule/.dPIJdtrule/.bgmomrule/.\[Sigma]brule//org*)


(* ::Input:: *)
(*AutomaticRules[\[Pi]a,MakeRule[{ParamD[t][\[Pi]a[]],\[Pi]adot}]]*)


(* ::Input:: *)
(*dGdt=ParamD[t][G1]/.MakeRule[{ParamD[t][\[Gamma]0[]],0}]/.MakeRule[{ParamD[t][\[Gamma]1[]],0}]/.MakeRule[{ParamD[t][\[Gamma]2[]],0}]/.MakeRule[{ParamD[t][\[Gamma]3[]],0}]/.MakeRule[{ParamD[t][\[Gamma]4[]],0}]/.MakeRule[{ParamD[t][\[Gamma]5[]],0}]/.MakeRule[{ParamD[t][\[Gamma]6[]],0}]/.MakeRule[{ParamD[t][\[CapitalPi]0[]],0}]/.MakeRule[{ParamD[t][\[CapitalPi]3[]],0}]/.MakeRule[{ParamD[t][\[CapitalPi]4[]],0}]/.MakeRule[{ParamD[t][\[CapitalPi]5[]],0}]/.MakeRule[{ParamD[t][\[CapitalPi]6[]],0}]/.MakeRule[{ParamD[t][\[CapitalPi]1[]],0}]/.MakeRule[{ParamD[t][\[CapitalPi]2[]],0}]/.MakeRule[{\[Sigma][-i,-j]\[Sigma][i,j],\[Sigma]b[]^2}]/.\[Sigma]brule;*)
(*(*Time derivative of second generating function.*)*)


(* ::Input:: *)
(*dGdt1=dGdt/.dhijdtrule/.dPIJdtrule/.bgmomrule/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule/.\[Sigma]brule/.bgconstraintrule;*)
(*(*Time derivative of second generating function.*)*)


(* ::Input:: *)
(*(*By comparing the projections of two definitions of time derivative of background momentum given below, we can calculate the time derivatives of components of shear.*)*)


(* ::Input:: *)
(*dPIJdt1=dPIJdt/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule/.\[Sigma]brule/.bgconstraintrule/.\[Sigma]brule//org*)
(*(*Time derivative of background momentum derived above. This do not involve derivative of components of shear.*)*)


(* ::Input:: *)
(*dPIJdtexp=ParamD[t][P[i,j]/.bgmomrule/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule]/.dhijdtrule/.de1Idtrule/.de2Idtrule/.dkIdtrule/.sheardecomposition/.A1rule/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule//org*)
(*(*Time derivative of background momentum derived from the definiton of background momentum. This involves time derivative of components of shear.*)*)


(* ::Text:: *)
(*Now let us equate both equations and consider their projection to different basis.*)


(* ::Input:: *)
(*(*Projection along Subscript[A1, ij]:*)*)


(* ::Input:: *)
(*eq0a=dPIJdt1 h[-i,-j]//org*)


(* ::Input:: *)
(*eq0b=dPIJdtexp h[-i,-j]/.bgconstraintrule//org*)


(* ::Input:: *)
(*(*Projection along Subscript[A2, ij]:*)*)


(* ::Input:: *)
(*eq\[Sigma]21=dPIJdt1 A2[-i,-j](*3/2*)/.A2rule//org*)


(* ::Input:: *)
(*eq\[Sigma]22=dPIJdtexp A2[-i,-j](*3/2*)/.A2rule//org*)


(* ::Input:: *)
(*d\[Sigma]2dt=ParamD[t][\[Sigma]2[]]/.Flatten[Solve[eq\[Sigma]22==eq\[Sigma]21,{ParamD[t][\[Sigma]2[]]}]]*)
(*(*d\[Sigma]2/dt*)*)


(* ::Input:: *)
(*(*Projection along Subscript[A3, ij]:*)*)


(* ::Input:: *)
(*eq\[Sigma]31=dPIJdt1 A3[-i,-j]/.A3rule//org*)


(* ::Input:: *)
(*eq\[Sigma]32=dPIJdtexp A3[-i,-j]/.A3rule//org*)


(* ::Input:: *)
(*d\[Sigma]3dt=ParamD[t][\[Sigma]3[]]/.Flatten[Solve[eq\[Sigma]32==eq\[Sigma]31,{ParamD[t][\[Sigma]3[]]}]]*)
(*(*d\[Sigma]3/dt*)*)


(* ::Input:: *)
(*(*Projection along Subscript[A4, ij]:*)*)


(* ::Input:: *)
(*eq\[Sigma]41=dPIJdt1 A4[-i,-j]/.A4rule//org*)


(* ::Input:: *)
(*eq\[Sigma]42=dPIJdtexp A4[-i,-j]/.A4rule//org*)


(* ::Input:: *)
(*d\[Sigma]4dt=ParamD[t][\[Sigma]4[]]/.Flatten[Solve[eq\[Sigma]42==eq\[Sigma]41,{ParamD[t][\[Sigma]4[]]}]]*)
(*(*d\[Sigma]4/dt*)*)


(* ::Input:: *)
(*(*Projection along Subscript[A5, ij]:*)*)


(* ::Input:: *)
(*eq\[Sigma]51=dPIJdt1 A5[-i,-j]/.A5rule//org*)


(* ::Input:: *)
(*eq\[Sigma]52=dPIJdtexp A5[-i,-j]/.A5rule//org*)


(* ::Input:: *)
(*d\[Sigma]5dt=ParamD[t][\[Sigma]5[]]/.Flatten[Solve[eq\[Sigma]52==eq\[Sigma]51,{ParamD[t][\[Sigma]5[]]}]]*)
(*(*d\[Sigma]5/dt*)*)


(* ::Input:: *)
(*(*Projection along Subscript[A6, ij]:*)*)


(* ::Input:: *)
(*eq\[Sigma]61=dPIJdt1 A6[-i,-j]/.A6rule//org*)


(* ::Input:: *)
(*eq\[Sigma]62=dPIJdtexp A6[-i,-j]/.A6rule//org*)


(* ::Input:: *)
(*d\[Sigma]6dt=ParamD[t][\[Sigma]6[]]/.Flatten[Solve[eq\[Sigma]62==eq\[Sigma]61,{ParamD[t][\[Sigma]6[]]}]]*)
(*(*d\[Sigma]6/dt*)*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*(*Returning to the calculation of time derivative of second generating function, we obtain,*)*)


(* ::Input:: *)
(*dGdt2=dGdt1/.MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}]/.MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}]/.MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}]/.MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}]/.MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}]/.MakeRule[{\[Pi]0[],\[Pi]0old1}]/.MakeRule[{\[Pi]1[],\[Pi]1old1}]/.MakeRule[{\[Pi]2[],\[Pi]2old1}]/.MakeRule[{\[Pi]3[],\[Pi]3old1}]/.MakeRule[{\[Pi]4[],\[Pi]4old1}]/.MakeRule[{\[Pi]5[],\[Pi]5old1}]/.MakeRule[{\[Pi]6[],\[Pi]6old1}]/.Flatten[sov];*)
(*(*Time derivative of the second generating function in terms of new variables.*)*)


(* ::Subsection:: *)
(*Full second-order Hamiltonian*)


(* ::Input:: *)
(*S2d=S2c+dG\[Gamma]dt1+dGdt2/.bgconstraintrule;*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*S2e=S2d/.MakeRule[{\[CapitalPi]3[],0}]/.MakeRule[{\[CapitalPi]4[],0}]/.MakeRule[{\[CapitalPi]5[],0}]/.MakeRule[{\[CapitalPi]6[],0}]//org;*)
(*(*The Hamiltonian that we obtained was huge. However, we can simplify it by noting that on the constraint surface, we can set terms proportional to momenta \[CapitalPi]3, \[CapitalPi]4, \[CapitalPi]5 and \[CapitalPi]6 to zero.*)*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*S2e*)


(* ::Input:: *)
(*Coefficient[S2e,\[Pi]0[]]*)


(* ::Subsubsection:: *)
(*Decoupling of the pure gauge and the gauge independent part:*)


(* ::Input:: *)
(*(*Here,we verify that the gauge independent part and the pure gauge part have decoupled. Hence,it is sufficient to focus our attention on the gauge independent part.To verify this,we check that the coupling between gauge independent and pure gauge configuration variables are zero.*)*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalGamma]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalGamma]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalGamma]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalGamma]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalPi]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalPi]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalPi]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]0[]\[CapitalPi]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalGamma]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalGamma]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalGamma]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalGamma]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalPi]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalPi]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalPi]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]0[]\[CapitalPi]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalGamma]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalGamma]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalGamma]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalGamma]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalPi]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalPi]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalPi]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]1[]\[CapitalPi]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalGamma]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalGamma]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalGamma]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalGamma]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalPi]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalPi]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalPi]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]1[]\[CapitalPi]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalGamma]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalGamma]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalGamma]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalGamma]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalPi]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalPi]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalPi]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalGamma]2[]\[CapitalPi]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalGamma]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalGamma]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalGamma]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalGamma]6[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalPi]3[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalPi]4[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalPi]5[]]//Simplify*)


(* ::Input:: *)
(*Coefficient[S2e,\[CapitalPi]2[]\[CapitalPi]6[]]//Simplify*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Text:: *)
(*The above analysis imply that the pure gauge part completely decouple from the gauge independent part. Hence in the Hamiltonian we can set terms proportional to pure gauge configuration variables to zero.*)


(* ::Input:: *)
(*S2f=S2e/.MakeRule[{\[CapitalGamma]3[],0}]/.MakeRule[{\[CapitalGamma]4[],0}]/.MakeRule[{\[CapitalGamma]5[],0}]/.MakeRule[{\[CapitalGamma]6[],0}]/.MakeRule[{\[CapitalPi]3[],0}]/.MakeRule[{\[CapitalPi]4[],0}]/.MakeRule[{\[CapitalPi]5[],0}]/.MakeRule[{\[CapitalPi]6[],0}];*)


(* ::Input:: *)
(*Coefficient[S2f,\[CapitalPi]3[]]*)


(* ::Subsection:: *)
(*Simplifying the Hamiltonian*)


(* ::Text:: *)
(*Second - order Hamiltonian in the present form have cross - terms between the configuration variable and the momenta.We can use the freedom in choosing some of the free parameters, namely, Subscript[A, ij]' s to eliminate these cross - terms.*)


(* ::Input:: *)
(*sdiagAij=Solve[{Coefficient[S2f,\[CapitalGamma]0[]\[CapitalPi]0[]]==0,Coefficient[S2f,\[CapitalGamma]1[]\[CapitalPi]0[]]==0,Coefficient[S2f,\[CapitalGamma]2[]\[CapitalPi]0[]]==0,Coefficient[S2f,\[CapitalGamma]1[]\[CapitalPi]1[]]==0,Coefficient[S2f,\[CapitalGamma]2[]\[CapitalPi]2[]]==0,Coefficient[S2f,\[CapitalGamma]2[]\[CapitalPi]1[]]==0},{A01[],A05[],A06[],A56[],A12[],A26[]}]//Simplify;*)
(*(*Demanding that the cross-terms vanish,we can fix the remaining six Subscript[A,ij]'s.*)*)


(* ::Input:: *)
(*(*Thus, we obtain the following expression for the remaining Subscript[A, ij]'s:*)*)


(* ::Input:: *)
(*A01v=A01[]/.Flatten[sdiagAij]/.bgconstraintrule//org;*)


(* ::Input:: *)
(*A05v=A05[]/.Flatten[sdiagAij]//org;(*;*)
(*A05v=%//org*)*)


(* ::Input:: *)
(*A06v=A06[]/.Flatten[sdiagAij]/.bgconstraintrule//org;(*;*)
(*A06v=%//org*)*)


(* ::Input:: *)
(*A12v=A12[]/.Flatten[sdiagAij]/.bgconstraintrule//Simplify//org;*)
(*(*A12v=%//org*)*)


(* ::Input:: *)
(*A26v=A26[]/.Flatten[sdiagAij]//org;(*;*)
(*A26v=%//org*)*)


(* ::Input:: *)
(*A56v=A56[]/.Flatten[sdiagAij]//org;(*;*)
(*A56v=%//org*)*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*(*The above solutions for remaining Subscript[A, ij]'s ensures that the following cross-terms also vanish.*)*)


(* ::Input:: *)
(*eq\[CapitalGamma]0\[CapitalPi]0=Coefficient[S2f,\[CapitalPi]0[]\[CapitalGamma]0[]]/.Flatten[sdiagAij]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]0\[CapitalPi]1=Coefficient[S2f,\[CapitalGamma]0[]\[CapitalPi]1[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]0\[CapitalPi]2=Coefficient[S2f,\[CapitalGamma]0[]\[CapitalPi]2[]]/.Flatten[sdiagAij]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]1\[CapitalPi]0=Coefficient[S2f,\[CapitalPi]0[]\[CapitalGamma]1[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]1\[CapitalPi]1=Coefficient[S2f,\[CapitalPi]1[]\[CapitalGamma]1[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]1\[CapitalPi]2=Coefficient[S2f,\[CapitalPi]2[]\[CapitalGamma]1[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]2\[CapitalPi]0=Coefficient[S2f,\[CapitalPi]0[]\[CapitalGamma]2[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]2\[CapitalPi]1=Coefficient[S2f,\[CapitalPi]1[]\[CapitalGamma]2[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalGamma]2\[CapitalPi]2=Coefficient[S2f,\[CapitalPi]2[]\[CapitalGamma]2[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalPi]0\[CapitalPi]1=Coefficient[S2f,\[CapitalPi]0[]\[CapitalPi]1[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalPi]0\[CapitalPi]2=Coefficient[S2f,\[CapitalPi]0[]\[CapitalPi]2[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Input:: *)
(*eq\[CapitalPi]1\[CapitalPi]2=Coefficient[S2f,\[CapitalPi]1[]\[CapitalPi]2[]]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}]//Simplify*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*(*Time derivatives of Subscript[A, ij]'s*)*)


(* ::Input:: *)
(*dA01dt=ParamD[t][A01v]/.bgconstraintrule/.dhijdtrule/.MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}]/.MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}]/.MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}]/.MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}]/.MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}]/.bgconstraintrule//org;*)


(* ::Input:: *)
(*dA05dt=ParamD[t][A05v]/.dhijdtrule/.MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}]/.MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}]/.MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}]/.MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}]/.MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}]/.bgconstraintrule//Simplify//org;*)


(* ::Input:: *)
(*dA06dt=ParamD[t][A06v]/.dhijdtrule/.MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}]/.MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}]/.MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}]/.MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}]/.MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}]/.bgconstraintrule(*//Simplify*)//org;*)


(* ::Input:: *)
(*dA12dt=ParamD[t][A12v]/.dhijdtrule/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule/.\[Sigma]brule/.MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}]/.MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}]/.MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}]/.MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}]/.MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}]/.bgconstraintrule(*//Simplify*)//org;*)


(* ::Input:: *)
(*dA26dt=ParamD[t][A26v]/.dhijdtrule/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule/.\[Sigma]brule/.MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}]/.MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}]/.MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}]/.MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}]/.MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}]/.bgconstraintrule//org;*)


(* ::Input:: *)
(*dA56dt=ParamD[t][A56v]/.dhijdtrule/.sheardecomposition/.A2rule/.A3rule/.A4rule/.A5rule/.A6rule/.\[Sigma]brule/.MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}]/.MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}]/.MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}]/.MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}]/.MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}]/.bgconstraintrule(*//Simplify*)//org;*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*(*Now,we substitute the new found Subscript[A,ij]'s and their derivatives in the Hamiltonian and set all remaining parameters to zero.*)*)
(*S2f/.Flatten[sCEFJ]/.MakeRule[{C0[],0}]/.MakeRule[{C5[],0}]/.MakeRule[{C6[],0}]/.MakeRule[{E0[],0}]/.MakeRule[{E5[],0}]/.MakeRule[{E6[],0}]/.MakeRule[{F0[],0}]/.MakeRule[{F5[],0}]/.MakeRule[{F6[],0}]/.MakeRule[{J0[],0}]/.MakeRule[{J5[],0}]/.MakeRule[{J6[],0}];*)
(*S2g=%/.MakeRule[{ParamD[t][A01[]],dA01dt}]/.MakeRule[{ParamD[t][A05[]],dA05dt}]/.MakeRule[{ParamD[t][A06[]],dA06dt}]/.MakeRule[{ParamD[t][A12[]],dA12dt}]/.MakeRule[{ParamD[t][A26[]],dA26dt}]/.MakeRule[{ParamD[t][A56[]],dA56dt}]/.MakeRule[{A01[],A01v}]/.MakeRule[{A05[],A05v}]/.MakeRule[{A06[],A06v}]/.MakeRule[{A12[],A12v}]/.MakeRule[{A26[],A26v}]/.MakeRule[{A56[],A56v}];(*//Simplify*)*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Subsection:: *)
(*Final Hamiltonian*)


(* ::Input:: *)
(*(*The final hamiltonian is given by*)*)
(*S2h = S2g/.bgconstraintrule//org*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Subsubsection:: *)
(*FLRW limit*)


(* ::Text:: *)
(*Up on setting shear to be zero. One obtains the Hamiltonian for perturbations in the FLRW spacetime. Scalar part of the Hamiltonian can be compared with Eq. 54 of Langlois, 1994 after noting that P = Sqrt[4 \[Kappa]] \[CapitalPi]0[], Q = \[CapitalGamma]0[]/Sqrt[4\[Kappa]] and Subscript[\[Pi], \[Alpha]] = Deth[]^(1/6) Subscript[\[Pi], a]. Similarly tensor perturbations can be compared with Eq. 61.*)


(* ::Input:: *)
(*S2h/.MakeRule[{\[Sigma]2[],0}]/.MakeRule[{\[Sigma]3[],0}]/.MakeRule[{\[Sigma]4[],0}]/.MakeRule[{\[Sigma]5[],0}]/.MakeRule[{\[Sigma]6[],0}]*)


(* ::Subsubsection:: *)
(*Comparison with expressions in Pereira, Pitrou and Uzan, arXiv:0707.0736v1*)


(* ::Input:: *)
(*dA2dtrule=MakeRule[{ParamD[t][A2[-i,-j]],(\[Kappa] h[-i, -j] \[Pi]a[])/(3 Sqrt[6] Deth[]^(1/3))-(\[Kappa] kv[-i] kv[-j] \[Pi]a[])/(Sqrt[6] Deth[]^(1/3) k[]^2)+1/3 h[-i, -j] \[Sigma]2[]+(kv[-i] kv[-j] \[Sigma]2[])/k[]^2-(e1[-j] kv[-i] \[Sigma]3[])/(Sqrt[3] k[])-(e1[-i] kv[-j] \[Sigma]3[])/(Sqrt[3] k[])-(e2[-j] kv[-i] \[Sigma]4[])/(Sqrt[3] k[])-(e2[-i] kv[-j] \[Sigma]4[])/(Sqrt[3] k[])-(e1[-i] e1[-j] \[Sigma]5[])/Sqrt[3]+(e2[-i] e2[-j] \[Sigma]5[])/Sqrt[3]-(e1[-j] e2[-i] \[Sigma]6[])/Sqrt[3]-(e1[-i] e2[-j] \[Sigma]6[])/Sqrt[3]}]*)


(* ::Input:: *)
(*dA3dtrule=MakeRule[{ParamD[t][A3[-i,-j]],-((\[Kappa] e1[-j] kv[-i] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3) k[]))-(\[Kappa] e1[-i] kv[-j] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3) k[])+(e1[-j] kv[-i] \[Sigma]2[])/(2 Sqrt[3] k[])+(e1[-i] kv[-j] \[Sigma]2[])/(2 Sqrt[3] k[])+(2 kv[-i] kv[-j] \[Sigma]3[])/k[]^2+(e1[-j] kv[-i] \[Sigma]5[])/(2 k[])+(e1[-i] kv[-j] \[Sigma]5[])/(2 k[])+(e2[-j] kv[-i] \[Sigma]6[])/(2 k[])+(e2[-i] kv[-j] \[Sigma]6[])/(2 k[])}]*)


(* ::Input:: *)
(*dA4dtrule=MakeRule[{ParamD[t][A4[-i,-j]],-((\[Kappa] e2[-j] kv[-i] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3) k[]))-(\[Kappa] e2[-i] kv[-j] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3) k[])+(e2[-j] kv[-i] \[Sigma]2[])/(2 Sqrt[3] k[])+(e2[-i] kv[-j] \[Sigma]2[])/(2 Sqrt[3] k[])+(2 kv[-i] kv[-j] \[Sigma]4[])/k[]^2-(e2[-j] kv[-i] \[Sigma]5[])/(2 k[])-(e2[-i] kv[-j] \[Sigma]5[])/(2 k[])+(e1[-j] kv[-i] \[Sigma]6[])/(2 k[])+(e1[-i] kv[-j] \[Sigma]6[])/(2 k[])}]*)


(* ::Input:: *)
(*dA5dtrule=MakeRule[{ParamD[t][A5[-i,-j]],-((\[Kappa] e1[-i] e1[-j] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3)))+(\[Kappa] e2[-i] e2[-j] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3))-(e1[-i] e1[-j] \[Sigma]2[])/Sqrt[3]+(e2[-i] e2[-j] \[Sigma]2[])/Sqrt[3]+(e1[-j] kv[-i] \[Sigma]3[])/k[]+(e1[-i] kv[-j] \[Sigma]3[])/k[]-(e2[-j] kv[-i] \[Sigma]4[])/k[]-(e2[-i] kv[-j] \[Sigma]4[])/k[]+e1[-i] e1[-j] \[Sigma]5[]+e2[-i] e2[-j] \[Sigma]5[]}]*)


(* ::Input:: *)
(*dA6dtrule=MakeRule[{ParamD[t][A6[-i,-j]],-((\[Kappa] e1[-j] e2[-i] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3)))-(\[Kappa] e1[-i] e2[-j] \[Pi]a[])/(3 Sqrt[2] Deth[]^(1/3))-(e1[-j] e2[-i] \[Sigma]2[])/Sqrt[3]-(e1[-i] e2[-j] \[Sigma]2[])/Sqrt[3]+(e2[-j] kv[-i] \[Sigma]3[])/k[]+(e2[-i] kv[-j] \[Sigma]3[])/k[]+(e1[-j] kv[-i] \[Sigma]4[])/k[]+(e1[-i] kv[-j] \[Sigma]4[])/k[]+e1[-i] e1[-j] \[Sigma]6[]+e2[-i] e2[-j] \[Sigma]6[]}]*)


(* ::Input:: *)
(*d\[Sigma]2dtrule=MakeRule[{ParamD[t][\[Sigma]2[]],d\[Sigma]2dt}];*)
(*d\[Sigma]3dtrule=MakeRule[{ParamD[t][\[Sigma]3[]],d\[Sigma]3dt}];*)
(*d\[Sigma]4dtrule=MakeRule[{ParamD[t][\[Sigma]4[]],d\[Sigma]4dt}];*)
(*d\[Sigma]5dtrule=MakeRule[{ParamD[t][\[Sigma]5[]],d\[Sigma]5dt}];*)
(*d\[Sigma]6dtrule=MakeRule[{ParamD[t][\[Sigma]6[]],d\[Sigma]6dt}];*)


(* ::Input:: *)
(*appbya=Deth[]^(1/6) ParamD[t][Deth[]^(1/6) ParamD[t][Deth[]^(1/6)]/.dhijdtrule/.sheardecomposition]/Deth[]^(1/6)/.dhijdtrule/.sheardecomposition/.dA2dtrule/.dA3dtrule/.dA4dtrule/.dA5dtrule/.dA6dtrule/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule/.bgconstraintrule//org*)


(* ::Input:: *)
(*\[Rho]=P\[Phi][]^2/(2 Deth[])+V[\[Phi][]];*)


(* ::Input:: *)
(*DefTensor[\[Sigma]p[],{M3,t}]*)
(*DefTensor[\[Sigma]v1[],{M3,t}]*)
(*DefTensor[\[Sigma]v2[],{M3,t}]*)
(*DefTensor[\[Sigma]Tp[],{M3,t}]*)
(*DefTensor[\[Sigma]Tc[],{M3,t}]*)


(* ::Input:: *)
(*\[Sigma]prule=MakeRule[{\[Sigma]p[],Deth[]^(1/6) Sqrt[2/3]\[Sigma]2[]}]*)


(* ::Input:: *)
(*\[Sigma]v1rule=MakeRule[{\[Sigma]v1[],Deth[]^(1/6) \[Sigma]3[]/Sqrt[2]}]*)


(* ::Input:: *)
(*\[Sigma]v2rule=MakeRule[{\[Sigma]v2[],Deth[]^(1/6) \[Sigma]4[]/Sqrt[2]}]*)


(* ::Input:: *)
(*\[Sigma]Tprule=MakeRule[{\[Sigma]Tp[],Deth[]^(1/6) \[Sigma]5[]}]*)
(*\[Sigma]Tcrule=MakeRule[{\[Sigma]Tc[],Deth[]^(1/6) \[Sigma]6[]}]*)


(* ::Input:: *)
(*onebytwo\[ScriptCapitalH]minus\[Sigma]p=(2 (-\[Kappa] \[Pi]a[]/6/Deth[]^(1/6))+\[Sigma]p[])/((4\[Kappa] \[Rho] Deth[]^(1/3))/3+2/3  (2\[Sigma]v1[]^2+2\[Sigma]v2[]^2+\[Sigma]Tp[]^2+\[Sigma]Tc[]^2))/.\[Sigma]prule/.\[Sigma]v1rule/.\[Sigma]v2rule/.\[Sigma]Tprule/.\[Sigma]Tcrule//Simplify*)


(* ::Input:: *)
(*\[ScriptCapitalF]1=onebytwo\[ScriptCapitalH]minus\[Sigma]p;*)


(* ::Input:: *)
(*d\[ScriptCapitalF]1dt=ParamD[t][\[ScriptCapitalF]1]/.dhijdtrule/.sheardecomposition/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule//org;*)


(* ::Input:: *)
(*zsppbyzs=appbya - Deth[]^(1/3) D[V[\[Phi][]],{\[Phi][],2}]+Deth[]^(-1/6) ((2\[Kappa] P\[Phi][]^2)/Deth[]^(1/3))d\[ScriptCapitalF]1dt+Deth[]^(-1/6) ParamD[t][(2\[Kappa] P\[Phi][]^2)/Deth[]^(1/3)]\[ScriptCapitalF]1/.dhijdtrule/.sheardecomposition/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule//org*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]0[]^2 :*)(Coefficient[S2g,\[CapitalGamma]0[]^2]-(-zsppbyzs+appbya+Deth[]^(1/3) k[]^2) 1/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule//FullSimplify*)


(* ::Input:: *)
(*zpppbyzp = (appbya + 2 \[Sigma]Tc[]^2+1/Deth[]^(1/6) ParamD[t][Deth[]^(1/3) \[Sigma]p[]/.\[Sigma]prule]+1/Deth[]^(1/6) ParamD[t][2 Deth[]^(1/3) \[Sigma]Tp[]^2/.\[Sigma]Tprule]\[ScriptCapitalF]1+1/Deth[]^(1/6)*2 Deth[]^(1/3) \[Sigma]Tp[]^2*d\[ScriptCapitalF]1dt)/.\[Sigma]Tcrule/.\[Sigma]Tprule/.dhijdtrule/.sheardecomposition/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule//org*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]1[]^2 :*)Simplify[(Coefficient[S2g,\[CapitalGamma]1[]^2]-(-zpppbyzp+appbya+Deth[]^(1/3) k[]^2) 1/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->500]*)


(* ::Input:: *)
(*zcppbyzc = appbya + 2 \[Sigma]Tp[]^2+1/Deth[]^(1/6) ParamD[t][Deth[]^(1/3) \[Sigma]p[]/.\[Sigma]prule]+1/Deth[]^(1/6) ParamD[t][2 Deth[]^(1/3) \[Sigma]Tc[]^2/.\[Sigma]Tcrule]\[ScriptCapitalF]1+1/Deth[]^(1/6)*2 Deth[]^(1/3) \[Sigma]Tc[]^2*d\[ScriptCapitalF]1dt/.\[Sigma]Tprule/.\[Sigma]Tcrule/.dhijdtrule/.sheardecomposition/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule//org;*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]2[]^2 :*)FullSimplify[(Coefficient[S2h,\[CapitalGamma]2[]^2]-(-zcppbyzc+appbya+Deth[]^(1/3) k[]^2) 1/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->1000]*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*couplnvp=(1/Deth[]^(1/6) ParamD[t][(2Sqrt[\[Kappa]] P\[Phi][]\[Sigma]Tp[])/.\[Sigma]Tprule]\[ScriptCapitalF]1+1/Deth[]^(1/6) (2Sqrt[\[Kappa]] P\[Phi][]\[Sigma]Tp[])d\[ScriptCapitalF]1dt)/.\[Sigma]Tprule/.\[Sigma]Tcrule/.dhijdtrule/.sheardecomposition/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule//org*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]0[]\[CapitalGamma]1[] :*)FullSimplify[(Coefficient[S2g,\[CapitalGamma]0[]\[CapitalGamma]1[]]+couplnvp*2/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->1000]*)


(* ::Input:: *)
(*couplnvc=1/Deth[]^(1/6) ParamD[t][(2Sqrt[\[Kappa]] P\[Phi][]\[Sigma]Tc[])/.\[Sigma]Tcrule]\[ScriptCapitalF]1+1/Deth[]^(1/6) (2Sqrt[\[Kappa]] P\[Phi][]\[Sigma]Tc[])d\[ScriptCapitalF]1dt/.\[Sigma]Tprule/.\[Sigma]Tcrule/.dhijdtrule/.sheardecomposition/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule//org*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]0[]\[CapitalGamma]2[] :*)FullSimplify[(Coefficient[S2g,\[CapitalGamma]0[]\[CapitalGamma]2[]]+couplnvc*2/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->1000]*)


(* ::Input:: *)
(*couplnpc=2\[Sigma]Tp[]\[Sigma]Tc[]-1/Deth[]^(1/6) (2Deth[]^(1/3) \[Sigma]Tp[]\[Sigma]Tc[])d\[ScriptCapitalF]1dt-1/Deth[]^(1/6) ParamD[t][(2Deth[]^(1/3) \[Sigma]Tp[]\[Sigma]Tc[])/.\[Sigma]Tprule/.\[Sigma]Tcrule]\[ScriptCapitalF]1/.\[Sigma]Tprule/.\[Sigma]Tcrule/.dhijdtrule/.sheardecomposition/.d\[Sigma]2dtrule/.d\[Sigma]3dtrule/.d\[Sigma]4dtrule/.d\[Sigma]5dtrule/.d\[Sigma]6dtrule//org;*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]1[]\[CapitalGamma]2[] :*)FullSimplify[(Coefficient[S2g,\[CapitalGamma]1[]\[CapitalGamma]2[]]-couplnpc*2/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->1000]*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Text:: *)
(* *)


(* ::Subsubsection:: *)
(*Comparison with our paper*)


(* ::Text:: *)
(*The Hamiltonian obtained above can be suitably massaged to write it in the form given in the accompanying paper.In what follows, we verify that the Hamiltonian agrees with the expression given in the paper.*)


(* ::Input:: *)
(*\[ScriptCapitalF]1//org*)


(* ::Input:: *)
(*\[ScriptCapitalG]2=d\[Sigma]2dt//org*)


(* ::Input:: *)
(*\[ScriptCapitalG]3=d\[Sigma]3dt//org*)


(* ::Input:: *)
(*\[ScriptCapitalG]4=d\[Sigma]4dt//org*)


(* ::Input:: *)
(*\[ScriptCapitalG]5=d\[Sigma]5dt//org*)


(* ::Input:: *)
(*\[ScriptCapitalG]6=d\[Sigma]6dt//org*)


(* ::Input:: *)
(*\[ScriptCapitalF]2=((3\[Kappa] V[\[Phi][]])/Deth[]^(1/6)-(\[Kappa]^2 \[Pi]a[]^2)/(3 Deth[]^(5/6))+(\[Kappa] \[Pi]a[]\[Sigma]2[])/(2Sqrt[6] Deth[]^(1/2))+Sqrt[3/2] \[ScriptCapitalG]2/Deth[]^(1/6)-\[ScriptCapitalF]1((\[Kappa]^2 P\[Phi][]^2 \[Pi]a[])/Deth[]^(8/6)+2\[Sigma]3[]\[ScriptCapitalG]3+2\[Sigma]4[]\[ScriptCapitalG]4+2\[Sigma]5[]\[ScriptCapitalG]5+2\[Sigma]6[]\[ScriptCapitalG]6))/(2\[Kappa] \[Rho]+\[Sigma]3[]^2+\[Sigma]4[]^2+\[Sigma]5[]^2+\[Sigma]6[]^2);*)


(* ::Input:: *)
(*\[ScriptCapitalF]2-d\[ScriptCapitalF]1dt/.bgconstraintrule//Simplify*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*U00=Deth[]^(1/3) D[V[\[Phi][]],{\[Phi][],2}]-(2\[Kappa] P\[Phi][]^2)/Deth[]^(1/2) \[ScriptCapitalF]2-(2\[Kappa] \[ScriptCapitalF]1)/Deth[]^(1/6) ((\[Kappa] P\[Phi][]^2 \[Pi]a[])/(3 Deth[]^(4/6))-2Deth[]^(1/6) P\[Phi][]D[V[\[Phi][]],\[Phi][]]);*)


(* ::Input:: *)
(*(Coefficient[S2g,\[CapitalGamma]0[]^2]-1/(2Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]) U00-1/(2Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]) Deth[]^(1/3) k[]^2)/.bgconstraintrule//Simplify*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*U11=-2 Deth[]^(1/3) \[Sigma]6[]^2+(\[Kappa] \[Pi]a[] \[Sigma]2[])/Sqrt[6]-Deth[]^(1/3) Sqrt[2/3]\[ScriptCapitalG]2+4/3 \[Kappa] Deth[]^(1/6) \[Pi]a[]\[Sigma]5[]^2 \[ScriptCapitalF]1-4Deth[]^(1/2) \[Sigma]5[]\[ScriptCapitalF]1 \[ScriptCapitalG]5-2 Deth[]^(1/2) \[Sigma]5[]^2 \[ScriptCapitalF]2;*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*(Coefficient[S2g,\[CapitalGamma]1[]^2]-1/(2Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]) U11-1/(2Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]) Deth[]^(1/3) k[]^2)/.bgconstraintrule//Simplify*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*U22=-2 Deth[]^(1/3) \[Sigma]5[]^2+(\[Kappa] \[Pi]a[] \[Sigma]2[])/Sqrt[6]-Deth[]^(1/3) Sqrt[2/3]\[ScriptCapitalG]2+4/3 \[Kappa] Deth[]^(1/6) \[Pi]a[]\[Sigma]6[]^2 \[ScriptCapitalF]1-4Deth[]^(1/2) \[Sigma]6[]\[ScriptCapitalF]1 \[ScriptCapitalG]6-2 Deth[]^(1/2) \[Sigma]6[]^2 \[ScriptCapitalF]2;*)


(* ::Input:: *)
(*TimeUsed[]*)


(* ::Input:: *)
(*(Coefficient[S2g,\[CapitalGamma]2[]^2]-1/(2Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]) U22-1/(2Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]) Deth[]^(1/3) k[]^2)/.bgconstraintrule//Simplify*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*U01=-((-((\[Kappa]^(3/2) P\[Phi][]\[Pi]a[]\[Sigma]5[])/(3 Deth[]^(1/3)))+2Sqrt[\[Kappa]]P\[Phi][]\[ScriptCapitalG]5-2Sqrt[\[Kappa]] Deth[]^(1/2) \[Sigma]5[]D[V[\[Phi][]],\[Phi][]])\[ScriptCapitalF]1+2Sqrt[\[Kappa]]P\[Phi][]\[Sigma]5[]\[ScriptCapitalF]2);*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]0[]\[CapitalGamma]1[] :*)FullSimplify[(Coefficient[S2g,\[CapitalGamma]0[]\[CapitalGamma]1[]]-U01*2/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->1000]*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*U02=((\[Kappa]^(3/2) P\[Phi][]\[Pi]a[]\[Sigma]6[])/(3 Deth[]^(1/3))-2Sqrt[\[Kappa]]P\[Phi][]\[ScriptCapitalG]6+2Sqrt[\[Kappa]] Deth[]^(1/2) \[Sigma]6[]D[V[\[Phi][]],\[Phi][]])\[ScriptCapitalF]1-2Sqrt[\[Kappa]]P\[Phi][]\[Sigma]6[]\[ScriptCapitalF]2;*)


(* ::Input:: *)
(*(*Comparing coefficients of \[CapitalGamma]0[]\[CapitalGamma]1[] :*)FullSimplify[(Coefficient[S2g,\[CapitalGamma]0[]\[CapitalGamma]2[]]-U02*2/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->1000]*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*U12=(4/3 \[Kappa] Deth[]^(1/6) \[Pi]a[]\[Sigma]5[]\[Sigma]6[]-2Deth[]^(1/2) \[Sigma]6[]\[ScriptCapitalG]5-2Deth[]^(1/2) \[Sigma]5[]\[ScriptCapitalG]6)\[ScriptCapitalF]1-2Deth[]^(1/2) \[Sigma]5[]\[Sigma]6[]\[ScriptCapitalF]2+2Deth[]^(1/3) \[Sigma]5[]\[Sigma]6[];*)


(* ::Input:: *)
(*FullSimplify[(Coefficient[S2g,\[CapitalGamma]1[]\[CapitalGamma]2[]]-U12*2/(2 Deth[]^(1/6)) Deth[]^(1/3)/(4\[Kappa]))/.bgconstraintrule,TimeConstraint->1000]*)


(* ::Text:: *)
(* *)


(* ::Subsection:: *)
(*Some other expressions*)


(* ::Subsubsection:: *)
(*Second-order Hamiltonian in terms of Subscript[\[Gamma], i] and \[Pi]^i*)


(* ::Input:: *)
(*S2\[Gamma]\[Pi] = S2b+dG\[Gamma]dt//Simplify//org*)


(* ::Text:: *)
(* *)


(* ::Subsubsection:: *)
(*Pure gauge variables*)


(* ::Input:: *)
(*\[CapitalGamma]3new1/.C0[]->0/.C5[]->0/.C6[]->0//FullSimplify*)


(* ::Input:: *)
(*\[CapitalGamma]4new1/.E0[]->0/.E5[]->0/.E6[]->0//FullSimplify*)


(* ::Input:: *)
(*\[CapitalGamma]5new1/.F0[]->0/.F5[]->0/.F6[]->0//FullSimplify*)


(* ::Input:: *)
(*\[CapitalGamma]6new1/.J0[]->0/.J5[]->0/.J6[]->0//FullSimplify*)


(* ::Text:: *)
(* *)


(* ::Subsubsection:: *)
(*Gauge invariant momenta*)


(* ::Text:: *)
(*It can be derived from the generating function. In order to simplify the expressions, we start from a simplified form of generating function.*)


(* ::Input:: *)
(*Gnew = \[CapitalGamma]0new1 \[CapitalPi]0[]+\[CapitalGamma]1new1 \[CapitalPi]1[]+\[CapitalGamma]2new1 \[CapitalPi]2[]+\[CapitalGamma]3new1 \[CapitalPi]3[]+\[CapitalGamma]4new1 \[CapitalPi]4[]+\[CapitalGamma]5new1 \[CapitalPi]5[]+\[CapitalGamma]6new1 \[CapitalPi]6[]+A00[]\[Gamma]0[]\[Gamma]0[]+2A01[]\[Gamma]0[]\[Gamma]1[]+2A02[]\[Gamma]0[]\[Gamma]2[]+2A03[]\[Gamma]0[]\[Gamma]3[]+2A04[]\[Gamma]0[]\[Gamma]4[]+2A05[]\[Gamma]0[]\[Gamma]5[]+2A06[]\[Gamma]0[]\[Gamma]6[]+A11[]\[Gamma]1[]\[Gamma]1[]+2A12[]\[Gamma]1[]\[Gamma]2[]+2A13[]\[Gamma]1[]\[Gamma]3[]+2A14[]\[Gamma]1[]\[Gamma]4[]+2A15[]\[Gamma]1[]\[Gamma]5[]+2A16[]\[Gamma]1[]\[Gamma]6[]+A22[]\[Gamma]2[]\[Gamma]2[]+2A23[]\[Gamma]2[]\[Gamma]3[]+2A24[]\[Gamma]2[]\[Gamma]4[]+2A25[]\[Gamma]2[]\[Gamma]5[]+2A26[]\[Gamma]2[]\[Gamma]6[]+A33[]\[Gamma]3[]\[Gamma]3[]+2A34[]\[Gamma]3[]\[Gamma]4[]+2A35[]\[Gamma]3[]\[Gamma]5[]+2A36[]\[Gamma]3[]\[Gamma]6[]+A44[]\[Gamma]4[]\[Gamma]4[]+2A45[]\[Gamma]4[]\[Gamma]5[]+2A46[]\[Gamma]4[]\[Gamma]6[]+A55[]\[Gamma]5[]\[Gamma]5[]+2A56[]\[Gamma]5[]\[Gamma]6[]+A66[]\[Gamma]6[]\[Gamma]6[]/.Flatten[sAij]/.Flatten[sdiagAij]/.C0[]->0/.C5[]->0/.C6[]->0/.E0[]->0/.E5[]->0/.E6[]->0/.F0[]->0/.F5[]->0/.F6[]->0/.J0[]->0/.J5[]->0/.J6[]->0;*)


(* ::Input:: *)
(*\[Pi]0oldnew=D[Gnew,\[Gamma]0[]]//org;*)


(* ::Input:: *)
(*\[Pi]1oldnew=D[Gnew,\[Gamma]1[]]//org;*)


(* ::Input:: *)
(*\[Pi]2oldnew=D[Gnew,\[Gamma]2[]]//org;*)


(* ::Input:: *)
(*\[Pi]3oldnew=D[Gnew,\[Gamma]3[]]//org;*)


(* ::Input:: *)
(*\[Pi]4oldnew=D[Gnew,\[Gamma]4[]]//org;*)


(* ::Input:: *)
(*\[Pi]5oldnew=D[Gnew,\[Gamma]5[]]//org;*)


(* ::Input:: *)
(*\[Pi]6oldnew=D[Gnew,\[Gamma]6[]]//org;*)


(* ::Input:: *)
(*(*Now,invert the expression for old momenta in terms of new momenta and old configuration variables to find the new momenta.*)*)


(* ::Input:: *)
(*spnnew=Solve[{\[Pi]0oldnew==\[Pi]0[],\[Pi]1oldnew==\[Pi]1[],\[Pi]2oldnew==\[Pi]2[],\[Pi]3oldnew==\[Pi]3[],\[Pi]4oldnew==\[Pi]4[],\[Pi]5oldnew==\[Pi]5[],\[Pi]6oldnew==\[Pi]6[]},{\[CapitalPi]0[],\[CapitalPi]3[],\[CapitalPi]4[],\[CapitalPi]5[],\[CapitalPi]6[],\[CapitalPi]1[],\[CapitalPi]2[]}]//Simplify;*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*\[CapitalPi]0new=\[CapitalPi]0[]/.Flatten[spnnew]//FullSimplify;*)


(* ::Input:: *)
(*(*Coefficient of Subscript[\[Gamma], 0] in Subscript[\[CapitalPi], 0]:*)Coefficient[\[CapitalPi]0new,\[Gamma]0[]]+3 P\[Phi][]^2/(4Deth[]^(1/6) (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))//Simplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]0new,\[Gamma]1[]]-(-((3 Sqrt[3] Sqrt[\[Kappa]] P\[Phi][]^3)/(2 Deth[]^(1/3) (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2))-(3 Deth[]^(1/3) P\[Phi][] \[Sigma]2[])/(2 Sqrt[2] Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))-(3 Sqrt[3] Deth[]^(2/3) P\[Phi][] \[Sigma]5[]^2)/(2 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(3 Sqrt[3] Deth[]^(2/3) P\[Phi][] \[Sigma]6[]^2)/(2 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(Sqrt[3] Deth[]^(5/6) Derivative[1][V][\[Phi][]])/(2 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])))//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]0new,\[Gamma]2[]]-((3 Sqrt[3/2] Sqrt[\[Kappa]] P\[Phi][]^3)/(2 Deth[]^(1/3) (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)+(3 Sqrt[3/2] Deth[]^(2/3) P\[Phi][] (\[Sigma]5[]^2+\[Sigma]6[]^2))/(2 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(Sqrt[3/2] (\[Kappa] P\[Phi][] \[Pi]a[]-2 Deth[]^(5/6) Derivative[1][V][\[Phi][]]))/(4 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])))//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]0new,\[Gamma]3[]]*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]0new,\[Gamma]4[]]*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]0new,\[Gamma]5[]]-((-3 Deth[]^(1/3) P\[Phi][] \[Sigma]5[])/(4 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])))//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]0new,\[Gamma]6[]]-(-((3 Deth[]^(1/3) P\[Phi][] \[Sigma]6[])/(4 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))))//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]0new,\[Pi]0[]]//FullSimplify*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*\[CapitalPi]1new=\[CapitalPi]1[]/.Flatten[spnnew]//FullSimplify;*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Gamma]0[]]+(3 Deth[]^(1/3) P\[Phi][] \[Sigma]5[])/(4 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))(*(3*18\[Sqrt](3 \[Kappa])Deth[]^(1/3)(\[Kappa] \[Pi]a[]+\[Sqrt]6Deth[]^(1/3)\[Sigma]2[])^11\[Sigma]5[]P\[Phi][])/(\[Kappa] 6^(4/2) Sqrt[2]Sqrt[6](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^12)*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Gamma]1[]]-((Sqrt[3/2] Deth[]^(5/6) \[Sigma]3[]^2)/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))-(Sqrt[3/2] Deth[]^(5/6) \[Sigma]4[]^2)/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))-(3 Sqrt[3] Deth[]^(1/6) P\[Phi][]^2 \[Sigma]5[])/(2 (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(3 Sqrt[3] Deth[]^(7/6) \[Sigma]5[] \[Sigma]6[]^2)/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)+(Sqrt[Deth[]] \[Sigma]5[])/(2 Sqrt[3] \[Kappa])-(3 Deth[]^(5/6) \[Sigma]2[] \[Sigma]5[])/(2 Sqrt[2] \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))-(3 Sqrt[3] Deth[]^(7/6) \[Sigma]5[]^3)/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2))(*1/(\[Kappa] 36 Sqrt[2]Sqrt[6](\[Kappa] \[Pi]a[] + Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^12)*(54Sqrt[2]Deth[]^(5/6)(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^11(\[Sigma]3[]^2-\[Sigma]4[]^2)-3*108*\[Kappa] Deth[]^(1/6)\[Sigma]5[]P\[Phi][]^2(\[Kappa] \[Pi]a[] + Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^10-324Deth[]^(7/6)(\[Kappa] \[Pi]a[]+Sqrt[6]\[Sigma]2[]Deth[]^(1/3))^10\[Sigma]5[]\[Sigma]6[]^2+36Deth[]^(1/2)\[Sigma]5[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^12-54Sqrt[6]Deth[]^(5/6)\[Sigma]2[]\[Sigma]5[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^11-3*108*Deth[]^(7/6)\[Sigma]5[]^3(\[Kappa] \[Pi]a[]+ Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^10)*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Gamma]2[]]-((Sqrt[3] Deth[]^(5/6) (-\[Sigma]3[]^2+\[Sigma]4[]^2))/(4 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))+(3 Sqrt[3/2] Deth[]^(1/6) P\[Phi][]^2 \[Sigma]5[])/(2 (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)+(3 Sqrt[3/2] Deth[]^(7/6) \[Sigma]5[] (\[Sigma]5[]^2+\[Sigma]6[]^2))/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(5 Sqrt[Deth[]] \[Sigma]5[])/(4 Sqrt[6] \[Kappa])+(3 Deth[]^(5/6) \[Sigma]2[] \[Sigma]5[])/(4 \[Kappa]^2 \[Pi]a[]+4 Sqrt[6] \[Kappa] Deth[]^(1/3) \[Sigma]2[]))(*1/(\[Kappa] 36Sqrt[12](\[Kappa] \[Pi]a[] + Sqrt[6]Deth[]^(1/3) \[Sigma]2[])^12)*(-54Deth[]^(5/6)(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^11(\[Sigma]3[]^2-\[Sigma]4[]^2)+162 \[Kappa] Deth[]^(1/6)Sqrt[2](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^10\[Sigma]5[]P\[Phi][]^2+162Sqrt[2]Deth[]^(7/6)(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^10\[Sigma]5[]\[Sigma]6[]^2-45Sqrt[2]Deth[]^(1/2)(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^12\[Sigma]5[]+54 Overscript[h, Overscript[~, ~]]^(5/6) \[Sigma]2 Sqrt[3](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^11\[Sigma]5[]+9*18*\[Sqrt]2Deth[]^(7/6)(\[Kappa] \[Pi]a[]+\[Sqrt]6Deth[]^(1/3)\[Sigma]2[])^10\[Sigma]5[]^3)*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Gamma]3[]]//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Gamma]4[]]//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Gamma]5[]]-(1/6 Deth[]^(1/6) \[Pi]a[]-(Sqrt[Deth[]] \[Sigma]2[])/(2 Sqrt[6] \[Kappa])-(3 Deth[]^(5/6) \[Sigma]5[]^2)/(4 \[Kappa]^2 \[Pi]a[]+4 Sqrt[6] \[Kappa] Deth[]^(1/3) \[Sigma]2[]))(*1/(\[Kappa] 36Sqrt[12](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^12)(12Sqrt[3]Deth[]^(1/6)(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^13-54Sqrt[2]Deth[]^(1/2)\[Sigma]2[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^12)+(3 Overscript[h, Overscript[~, ~]]^(5/6) \[Sigma]5^2)/(4 \[Kappa]^2 \[Pi]a+4 Sqrt[6] \[Kappa] Overscript[h, Overscript[~, ~]]^(1/3) \[Sigma]2)*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Gamma]6[]]//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]1new,\[Pi]5[]]//FullSimplify*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*\[CapitalPi]2new=\[CapitalPi]2[]/.Flatten[spnnew]/.bgconstraintrule//FullSimplify;*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Gamma]0[]]-(-((3 Deth[]^(1/3) P\[Phi][] \[Sigma]6[])/(4 Sqrt[\[Kappa]] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[]))))(*1/(8\[Kappa] Deth[]^(5/6)36Sqrt[2]\[Sigma]6[]^2)1/(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^5*-216Sqrt[2]Sqrt[\[Kappa]]Deth[]^(7/6)P\[Phi][]\[Sigma]6[]^3(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^4*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Gamma]1[]]-(-((3 Sqrt[3] Deth[]^(1/6) P\[Phi][]^2 \[Sigma]6[])/(2 (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2))-(3 Sqrt[3] Deth[]^(7/6) \[Sigma]5[]^2 \[Sigma]6[])/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(3 Sqrt[3] Deth[]^(7/6) \[Sigma]6[]^3)/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)+(Sqrt[Deth[]] \[Sigma]6[])/(2 Sqrt[3] \[Kappa])-(3 Deth[]^(5/6) \[Sigma]2[] \[Sigma]6[])/(Sqrt[2] (2 \[Kappa]^2 \[Pi]a[]+2 Sqrt[6] \[Kappa] Deth[]^(1/3) \[Sigma]2[]))+(Sqrt[3/2] Deth[]^(5/6) \[Sigma]3[] \[Sigma]4[])/(\[Kappa]^2 \[Pi]a[]+Sqrt[6] \[Kappa] Deth[]^(1/3) \[Sigma]2[]))(*1/(36Sqrt[2]\[Kappa])1/(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^5(-9*6Sqrt[6]Deth[]^(1/6)\[Kappa] \[Sigma]6[]P\[Phi][]^2(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^3-54Deth[]^(7/6)Sqrt[6]\[Sigma]6[]\[Sigma]5[]^2(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^3+6\[Sqrt]6Deth[]^(1/2)\[Sigma]6[](\[Kappa] \[Pi]a[]+\[Sqrt]6Deth[]^(1/3)\[Sigma]2[])^5-54Deth[]^(5/6)\[Sigma]2[]\[Sigma]6[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^4-54Sqrt[6]Deth[]^(7/6)\[Sigma]6[]^3(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^3+36Deth[]^(5/6)Sqrt[3]\[Sigma]3[]\[Sigma]4[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^4)*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Gamma]2[]]-((3 Sqrt[3/2] Deth[]^(1/6) P\[Phi][]^2 \[Sigma]6[])/(2 (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(Sqrt[3] Deth[]^(5/6) \[Sigma]3[] \[Sigma]4[])/(2 \[Kappa]^2 \[Pi]a[]+2 Sqrt[6] \[Kappa] Deth[]^(1/3) \[Sigma]2[])+(3 Sqrt[3/2] Deth[]^(7/6) \[Sigma]6[] (\[Sigma]5[]^2+\[Sigma]6[]^2))/(2 \[Kappa] (\[Kappa] \[Pi]a[]+Sqrt[6] Deth[]^(1/3) \[Sigma]2[])^2)-(5 Sqrt[Deth[]] \[Sigma]6[])/(4 Sqrt[6] \[Kappa])+(3 Deth[]^(5/6) \[Sigma]2[] \[Sigma]6[])/(4 \[Kappa]^2 \[Pi]a[]+4 Sqrt[6] \[Kappa] Deth[]^(1/3) \[Sigma]2[]))(*1/(36Sqrt[2]\[Kappa])1/(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^5(3/2Deth[]^(1/6)36Sqrt[3]\[Kappa] P\[Phi][]^2\[Sigma]6[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^3-18Deth[]^(5/6)Sqrt[6]\[Sigma]3[]\[Sigma]4[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^4+54Sqrt[3]Deth[]^(7/6)\[Sigma]6[](\[Sigma]6[]^2+\[Sigma]5[]^2)(\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^3-15Sqrt[3]Deth[]^(1/2)\[Sigma]6[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^5+27Sqrt[2]Deth[]^(5/6)\[Sigma]2[]\[Sigma]6[](\[Kappa] \[Pi]a[]+Sqrt[6]Deth[]^(1/3)\[Sigma]2[])^4)*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Gamma]3[]]//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Gamma]4[]]//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Gamma]5[]]//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Gamma]6[]]-(1/6 Deth[]^(1/6) \[Pi]a[]-(Sqrt[Deth[]] \[Sigma]2[])/(2 Sqrt[6] \[Kappa])-(3 Deth[]^(5/6) \[Sigma]6[]^2)/(4 \[Kappa]^2 \[Pi]a[]+4 Sqrt[6] \[Kappa] Deth[]^(1/3) \[Sigma]2[]))(*1/(36\[Sqrt]2\[Kappa])1/(\[Kappa] \[Pi]a[]+\[Sqrt]6Deth[]^(1/3)\[Sigma]2[])^5(6\[Sqrt]2Deth[]^(1/6)(\[Kappa] \[Pi]a[]+\[Sqrt]6Deth[]^(1/3)\[Sigma]2[])^6-18Sqrt[3]Deth[]^(1/2)\[Sigma]2[](\[Kappa] \[Pi]a[]+\[Sqrt]6Deth[]^(1/3)\[Sigma]2[])^5)-(-((3 Overscript[h, Overscript[~, ~]]^(5/6) \[Sigma]6^2)/(4 \[Kappa]^2 \[Pi]a+4 Sqrt[6] \[Kappa] Overscript[h, Overscript[~, ~]]^(1/3) \[Sigma]2)))*)//FullSimplify*)


(* ::Input:: *)
(*Coefficient[\[CapitalPi]2new,\[Pi]6[]]//FullSimplify*)


(* ::Text:: *)
(* *)


(* ::Input:: *)
(*(*Here, we verify that the variables follows the commutation relations.*)*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]0new1,\[CapitalPi]0new,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]1new1,\[CapitalPi]1new,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]2new,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//Simplify*)


(* ::Input:: *)
(*PoissonBracket[\[CapitalGamma]2new1,\[CapitalPi]1new,{\[Gamma]0[],\[Gamma]1[],\[Gamma]2[],\[Gamma]3[],\[Gamma]4[],\[Gamma]5[],\[Gamma]6[]},{\[Pi]0[],\[Pi]1[],\[Pi]2[],\[Pi]3[],\[Pi]4[],\[Pi]5[],\[Pi]6[]}]//Simplify*)
