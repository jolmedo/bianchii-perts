# BianchiI-perts

This code uses xAct Mathematica package to implement, within a Hamiltonian formulation of general relativity minimally coupled to a single scalar field, gauge invariant linear perturbations on a Bianchi I spacetime (to first order in the perturbative expansion).